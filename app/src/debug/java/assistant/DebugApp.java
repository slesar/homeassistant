package assistant;

import assistant.utils.Logger;
import timber.log.Timber;

public class DebugApp extends App {

    @Override
    void initLogging() {
        Timber.plant(new Logger(true));
    }
}

package nest.ui.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;

import assistant.R;

public class ThermostatView extends View {

    private Paint paint;
    private Paint paintTarget;
    private Paint paintCurrent;

    private int width;
    private int height;
    private int size;
    private int centerX;
    private int centerY;

    private float tempCurrent = 22;
    private float tempTarget = 32;

    public ThermostatView(Context context) {
        super(context);
        init(null);
    }

    public ThermostatView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public ThermostatView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ThermostatView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        final float density = getContext().getResources().getDisplayMetrics().density;
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setStrokeWidth(density * 2);
        paint.setColor(0xffff8800);
        paint.setStrokeCap(Paint.Cap.ROUND);

        paintTarget = new Paint(Paint.ANTI_ALIAS_FLAG);
        paintTarget.setColor(0xccff0000);
        paintTarget.setStyle(Paint.Style.FILL);

        paintCurrent = new Paint(Paint.ANTI_ALIAS_FLAG);
        paintCurrent.setColor(0xccffff00);
        paintCurrent.setStyle(Paint.Style.FILL);

        if (attrs != null) {
            TypedArray a = getContext().getTheme().obtainStyledAttributes(attrs, R.styleable.ThermostatView, 0, 0);
            final int n = a.getIndexCount();
            for (int i = 0; i < n; i++) {
                final int attr = a.getIndex(i);
                switch (attr) {
                    case R.styleable.ThermostatView_barsColor:
                        paint.setColor(a.getColor(attr, paint.getColor()));
                        break;
                    case R.styleable.ThermostatView_barsWidth:
                        paint.setStrokeWidth(a.getDimension(attr, paint.getStrokeWidth()));
                        break;
                    case R.styleable.ThermostatView_currentColor:
                        paintCurrent.setColor(a.getColor(attr, paintCurrent.getColor()));
                        break;
                    case R.styleable.ThermostatView_targetColor:
                        paintTarget.setColor(a.getColor(attr, paintTarget.getColor()));
                        break;
                }
            }
            a.recycle();
        }
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);

        final int w = right-left;
        final int h = bottom-top;

        if (w != width || h != height) {
            width = w;
            height = h;

            size = Math.min(w - getPaddingLeft() - getPaddingRight(), h - getPaddingTop() - getPaddingBottom());
            centerX = (width - getPaddingLeft() - getPaddingRight()) / 2 + getPaddingLeft();
            centerY = (height - getPaddingTop() - getPaddingBottom()) / 2 + getPaddingTop();
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        final int savedColor = paint.getColor();
        if (!isEnabled()) {
            paint.setAlpha(100);
        }
        final float size = this.size/2F;
        canvas.translate(centerX, centerY);
        canvas.save();
        canvas.rotate(120);
        for (int i = 30; i <= 330; i += 3) {
            canvas.drawLine(size*.75F, 0, size, 0, paint);
            canvas.rotate(3);
        }
        canvas.restore();
        if (!isEnabled()) {
            paint.setColor(savedColor);
        }

        if (isEnabled()) {
            final int save = canvas.save();

            final float stroke = paint.getStrokeWidth();

            // current
            {
                final float angle = getCorrectAngle(tempCurrent / 32);
                canvas.rotate(angle);
                canvas.drawRect(size * .75F, -stroke * 3F, size * 1.01F, stroke * 3F, paintCurrent);
                canvas.rotate(-angle);
            }

            // target
            {
                final float angle = getCorrectAngle(tempTarget / 32);
                canvas.rotate(angle);
                canvas.drawRect(size * .68F, -stroke * 3F, size * 1.01F, stroke * 3F, paintTarget);
                canvas.rotate(-angle);
            }

            canvas.restoreToCount(save);
        }
    }

    public void setColorBars(int color) {
        paint.setColor(color);
        invalidate();
    }

    public void setColorCurrent(int color) {
        paintCurrent.setColor(color);
        invalidate();
    }

    public void setColorTarget(int color) {
        paintTarget.setColor(color);
        invalidate();
    }

    private static float getCorrectAngle(float fraction) {
        float angle = 120 + fraction * 300;
        angle = Math.min(angle, 420);
        angle = Math.max(angle, 120);
        return angle;
    }

    public void setCurrentTemperature(float tempCurrent) {
        this.tempCurrent = tempCurrent;
        invalidate();
    }

    public void setTargetTemperature(float tempTarget) {
        this.tempTarget = tempTarget;
        invalidate();
    }
}

package nest.service;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;

import java.util.HashSet;
import java.util.Random;

import javax.inject.Provider;

import assistant.model.EmptyBody;
import assistant.service.UserPreferencesService;
import assistant.utils.StringUtils;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import nest.api.auth.NestAuthApi;
import nest.model.account.AccessToken;
import timber.log.Timber;

public class NestAuthService {

    @Nullable
    private static String parseRequestCode(@NonNull String url) {
        try {
            return Uri.parse(url).getQueryParameter("code");
        } catch (Exception ignored) {
            return null;
        }
    }

    private boolean authenticated = false;

    private final HashSet<NestAuthListener> listeners = new HashSet<>();

    private final NestAuthApi nestAuthApi;
    private final NestApiService nestApiService;
    private final Provider<UserPreferencesService> prefs;

    private final String nestClientId;
    private final String nestClientSecret;
    private final String nestRedirectUrl;

    private final Firebase.AuthListener authListener = new Firebase.AuthListener() {
        @Override
        public void onAuthError(FirebaseError firebaseError) {
            Timber.v("Authentication failed with error: " + firebaseError.toString());
            setAuthenticatedState(false);
        }

        @Override
        public void onAuthSuccess(Object o) {
            Timber.v("Authentication succeeded.");
            setAuthenticatedState(true);
        }

        @Override
        public void onAuthRevoked(FirebaseError firebaseError) {
            Timber.v("Authentication revoked with error: " + firebaseError.toString());
            setAuthenticatedState(false);
            logIn();
        }
    };

    public NestAuthService(
            @NonNull NestAuthApi nestAuthApi,
            @NonNull NestApiService nestApiService,
            @NonNull Provider<UserPreferencesService> prefs,
            @NonNull String nestClientId,
            @NonNull String nestClientSecret,
            @NonNull String nestRedirectUrl) {
        this.nestAuthApi = nestAuthApi;
        this.nestApiService = nestApiService;
        this.prefs = prefs;
        this.nestClientId = nestClientId;
        this.nestClientSecret = nestClientSecret;
        this.nestRedirectUrl = nestRedirectUrl;
    }

    public boolean isAuthenticated() {
        return authenticated;
    }

    public void addListener(@NonNull NestAuthListener listener) {
        listeners.add(listener);
    }

    public void removeListener(@Nullable NestAuthListener listener) {
        if (listener == null) {
            listeners.clear();
        } else {
            listeners.remove(listener);
        }
    }

    public void logIn() {
        if (authenticated) {
            return;
        }
        final AccessToken token = prefs.get().getNestToken();
        if (token.isValid()) {
            authApiService(token);
        } else {
            Timber.v("Token is empty or expired");
            obtainRequestCode();
        }
    }

    public void logOut() {
        if (!authenticated) {
            return;
        }
        unAuthApiService();
    }

    public boolean inProgress() {
        // TODO
        return false;
    }

    public boolean parseRequestCodeRedirectUrl(@NonNull String url) {
        if (!url.startsWith(nestRedirectUrl)) {
            return false;
        }
        final String code = parseRequestCode(url);
        Timber.v("Got code: %s", code);
        if (StringUtils.isEmpty(code)) {
            // TODO handle error case
            return false;
        }
        obtainAccessToken(code);
        return true;
    }

    private void obtainRequestCode() {
        Timber.v("Obtain request code through Nest's website");
        final String url = String.format(NestAuthApi.REQUEST_CLIENT_CODE_URL, nestClientId,
                "app-state" + System.nanoTime() + "-" + new Random().nextInt());
        for (NestAuthListener listener : listeners) {
            listener.onRequestAuthCode(url);
        }
    }

    private void obtainAccessToken(@NonNull String code) {
        nestAuthApi.getAccessToken(code, nestClientId, nestClientSecret, new EmptyBody())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onAccessTokenSucceed, this::onAccessTokenFailed);
    }

    private void onAccessTokenSucceed(@NonNull AccessToken token) {
        Timber.v("Got access token: %s", token);
        prefs.get().setNestToken(token);
        authApiService(token);
    }

    private void onAccessTokenFailed(Throwable t) {
        Timber.e("Unable to get access token: %s", t);
        // TODO error
    }

    private void authApiService(@NonNull AccessToken token) {
        Timber.v("Authenticating. Token: " + token);
        nestApiService.auth(token, authListener);
    }

    private void unAuthApiService() {
        Timber.v("Un-authenticating");
        nestApiService.unAuth(() -> {
            prefs.get().setNestToken(null);
            setAuthenticatedState(false);
        });
    }

    private void setAuthenticatedState(boolean value) {
        if (authenticated != value) {
            authenticated = value;
            for (NestAuthListener listener : listeners) {
                listener.onAuthStateChanged(value);
            }
        }
    }

    public interface NestAuthListener {

        void onRequestAuthCode(@NonNull String requestUrl);

        void onAuthStateChanged(boolean authenticated);
    }
}

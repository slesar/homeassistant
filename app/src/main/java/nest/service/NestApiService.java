package nest.service;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;

import com.bluelinelabs.logansquare.LoganSquare;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.GenericTypeIndicator;
import com.firebase.client.ValueEventListener;

import org.json.JSONObject;

import java.util.Map;

import assistant.service.data.DataReceiver;
import dagger.Lazy;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import nest.model.FirebaseObject;
import nest.model.NestDevice;
import nest.model.account.AccessToken;
import nest.model.account.MetaData;
import nest.model.camera.NestCamera;
import nest.model.structure.NestStructure;
import nest.model.thermostat.NestThermostat;
import timber.log.Timber;

public class NestApiService {

    private static final boolean STORE_RAW_VALUE = true;

    @Nullable
    private static <T extends FirebaseObject> T parse(@NonNull Map<String, Object> map, @NonNull Class<T> clazz) {
        try {
            final String value = flatJsonMap(map);
            final T object = LoganSquare.parse(value, clazz);
            if (STORE_RAW_VALUE) {
                object.setRawValue(value);
            }
            return object;
        } catch (Exception ex) {
            Timber.e(ex, "Unable to parse object");
        }
        return null;
    }

    @NonNull
    private static String flatJsonMap(@NonNull Map<String, Object> map) {
        return new JSONObject(map).toString();
    }

    private static <T extends FirebaseObject> void parseObjectsFromMap(
            @NonNull Map<String, Object> map,
            @NonNull Class<T> parseClass,
            @NonNull OnFirebaseObjectParsedListener<T> listener) {
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            final Map<String, Object> value = (Map<String, Object>) entry.getValue();
            final T object = parse(value, parseClass);
            if (object != null) {
                Timber.v("Incoming: %s", value);
                listener.onObjectParsed(object);
            }
        }
    }

    private interface OnFirebaseObjectParsedListener<T extends FirebaseObject> {

        void onObjectParsed(@NonNull T object);
    }

    private final Observable<Firebase> firebaseObservable;

    private final DataReceiver dataReceiver;

    private final ValueEventListener firebaseListener = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            Observable
                    .<Boolean>create(subscriber -> {
                        for (Map.Entry<String, Object> entry : dataSnapshot.getValue(StringObjectMapIndicator.INSTANCE).entrySet()) {
                            notifyUpdateListeners(entry);
                        }
                        subscriber.onNext(true);
                        subscriber.onComplete();
                    })
                    .subscribeOn(Schedulers.computation())
                    .subscribe(value -> {}, error -> Timber.e(error, "Parse error"));
        }

        @Override
        public void onCancelled(FirebaseError firebaseError) {
            Timber.e("Cancelled: %s", firebaseError);
        }
    };

    public NestApiService(
            @NonNull Lazy<Firebase> firebase,
            @NonNull DataReceiver dataReceiver) {
        firebaseObservable = Observable
                .<Firebase>create(emitter -> {
                    emitter.onNext(firebase.get());
                    emitter.onComplete();
                })
                .replay()
                .autoConnect();

        this.dataReceiver = dataReceiver;
    }

    public void auth(@NonNull AccessToken token, @NonNull Firebase.AuthListener authListener) {
        firebaseObservable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(firebase -> {
                    firebase.addValueEventListener(firebaseListener);
                    firebase.auth(token.token, authListener);
                });
    }

    public void unAuth(@Nullable Runnable callback) {
        firebaseObservable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(firebase -> firebase.unauth((firebaseError, firebase1) -> {
                    firebase.removeEventListener(firebaseListener);
                    if (firebaseError == null && callback != null) {
                        callback.run();
                    }
                }));
        dataReceiver.clear();
    }

    public void sendRequest(String path, Object value) {
        firebaseObservable
                .doOnNext(firebase -> {
                    firebase.child(path).setValue(value, (firebaseError, firebase1) ->
                            Timber.v("Request completed, error: %s", firebaseError));
                })
                .subscribeOn(Schedulers.io())
                .subscribe();
    }

    @WorkerThread
    private void notifyUpdateListeners(Map.Entry<String, Object> entry) {
        final Map<String, Object> value = (Map<String, Object>) entry.getValue();
        switch (entry.getKey()) {
            case MetaData.PATH:
                updateMetaData(value);
                break;
            case NestStructure.PATH:
                updateStructures(value);
                break;
            case NestDevice.PATH:
                updateDevices(value);
                break;
        }
    }

    @WorkerThread
    private void updateMetaData(Map<String, Object> metadataMap) {
        final MetaData metaData = parse(metadataMap, MetaData.class);
        if (metaData != null) {
            Timber.v("Update: metadata");
            // TODO bring back
            //dataReceiver.putMetaData(metaData);
        }
    }

    @WorkerThread
    private void updateStructures(@NonNull Map<String, Object> structures) {
        parseObjectsFromMap(structures, NestStructure.class, dataReceiver::putHome);
    }

    @WorkerThread
    private void updateDevices(@NonNull Map<String, Object> devices) {
        for (Map.Entry<String, Object> entry : devices.entrySet()) {
            final Map<String, Object> value = (Map<String, Object>) entry.getValue();
            switch (entry.getKey()) {
                case NestThermostat.PATH:
                    updateThermostats(value);
                    break;
                case NestCamera.PATH:
                    updateCameras(value);
                    break;
            }
        }
    }

    @WorkerThread
    private void updateThermostats(@NonNull Map<String, Object> thermostatsMap) {
        parseObjectsFromMap(thermostatsMap, NestThermostat.class, dataReceiver::putThermostat);
    }

    @WorkerThread
    private void updateCameras(@NonNull Map<String, Object> camerasMap) {
        parseObjectsFromMap(camerasMap, NestCamera.class, dataReceiver::putCamera);
    }

    // Marker for Firebase to retrieve a strongly-typed collection
    private static class StringObjectMapIndicator extends GenericTypeIndicator<Map<String, Object>> {

        static final StringObjectMapIndicator INSTANCE = new StringObjectMapIndicator();
    }
}

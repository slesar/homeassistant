package nest.model;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public abstract class FirebaseObject {

    private String rawValue;

    public void setRawValue(@NonNull String value) {
        this.rawValue = value;
    }

    @Nullable
    public String getRawValue() {
        return rawValue;
    }

    @NonNull
    public abstract String getPath(@NonNull String objectId, @NonNull String field);
}

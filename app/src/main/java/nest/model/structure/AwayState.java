package nest.model.structure;


import android.support.annotation.NonNull;

public enum AwayState {

    /** State when the user has explicitly set the structure to "Away" state. */
    AWAY("away"),

    /** State when Nest has detected that the structure is not occupied. */
    AUTO_AWAY("auto-away"),

    /** State when the user is at home */
    HOME("home"),

    /** State for when the home/away status is unknown */
    UNKNOWN("unknown");

    @NonNull
    public static AwayState from(@NonNull String key) {
        for (AwayState value : values()) {
            if (value.key.equalsIgnoreCase(key)) {
                return value;
            }
        }
        return UNKNOWN;
    }

    private final String key;

    AwayState(@NonNull String key) {
        this.key = key;
    }

    @NonNull
    public String getKey() {
        return key;
    }
}

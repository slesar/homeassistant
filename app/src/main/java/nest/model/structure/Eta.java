package nest.model.structure;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.util.Map;

@JsonObject
public class Eta {

    @JsonField(name = "trip_id")
    public String tripID;

    @JsonField(name = "estimated_arrival_window_begin")
    public String estimatedArrivalWindowBegin;

    @JsonField(name = "estimated_arrival_window_end")
    public String estimatedArrivalWindowEnd;

    public Eta() {
    }

    public Eta(Map<String, Object> map) {

    }

    @Override
    public String toString() {
        return "Eta{" +
                "tripID='" + tripID + '\'' +
                ", estimatedArrivalWindowBegin='" + estimatedArrivalWindowBegin + '\'' +
                ", estimatedArrivalWindowEnd='" + estimatedArrivalWindowEnd + '\'' +
                '}';
    }
}

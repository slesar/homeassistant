package nest.model.structure;

import android.support.annotation.NonNull;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.util.List;

import nest.model.FirebaseObject;
import nest.retrofit.converter.AwayStateConverter;

@JsonObject
public class NestStructure extends FirebaseObject {

    public static final String PATH = "structures";

    public static final String FIELD_STRUCTURE_ID = "structure_id";
    public static final String FIELD_THERMOSTATS = "thermostats";
    public static final String FIELD_SMOKE_CO_ALARMS = "smoke_co_alarms";
    public static final String FIELD_CAMERAS = "cameras";
    public static final String FIELD_NAME = "name";
    public static final String FIELD_COUNTRY_CODE = "country_code";
    public static final String FIELD_PEAK_PERIOD_START_TIME = "peak_period_start_time";
    public static final String FIELD_PEAK_PERIOD_END_TIME = "peak_period_end_time";
    public static final String FIELD_TIME_ZONE = "time_zone";
    public static final String FIELD_AWAY = "away";
    public static final String FIELD_ETA = "eta";
    public static final String FIELD_WHERES = "wheres";

    @JsonField(name = FIELD_STRUCTURE_ID)
    public String structureId;

    @JsonField(name = FIELD_THERMOSTATS)
    public List<String> thermostatIds;

    @JsonField(name = FIELD_SMOKE_CO_ALARMS)
    public List<String> smokeCoAlarmIds;

    @JsonField(name = FIELD_CAMERAS)
    public List<String> cameraIds;

    @JsonField(name = FIELD_NAME)
    public String name;

    @JsonField(name = FIELD_COUNTRY_CODE)
    public String countryCode;

    @JsonField(name = FIELD_PEAK_PERIOD_START_TIME)
    public String peakPeriodStartTime;

    @JsonField(name = FIELD_PEAK_PERIOD_END_TIME)
    public String peakPeriodEndTime;

    @JsonField(name = FIELD_TIME_ZONE)
    public String timeZone;

    @JsonField(name = FIELD_AWAY, typeConverter = AwayStateConverter.class)
    public AwayState awayState;

    @JsonField(name = FIELD_ETA)
    public Eta eta;

    @JsonField(name = FIELD_WHERES)
    public List<String> wheres;

    public NestStructure() {
    }

    @NonNull
    @Override
    public String getPath(@NonNull String structureId, @NonNull String field) {
        return "/" + PATH + "/" + structureId + "/" + field;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final NestStructure structure = (NestStructure) o;

        return structureId != null ? structureId.equals(structure.structureId) : structure.structureId == null;
    }

    @Override
    public int hashCode() {
        return structureId.hashCode();
    }

    @Override
    public String toString() {
        return "NestStructure{" +
                "structureId='" + structureId + '\'' +
                ", thermostatIds=" + thermostatIds +
                ", smokeCoAlarmIds=" + smokeCoAlarmIds +
                ", cameraIds=" + cameraIds +
                ", name='" + name + '\'' +
                ", countryCode='" + countryCode + '\'' +
                ", peakPeriodStartTime='" + peakPeriodStartTime + '\'' +
                ", peakPeriodEndTime='" + peakPeriodEndTime + '\'' +
                ", timeZone='" + timeZone + '\'' +
                ", awayState=" + awayState +
                ", eta=" + eta +
                ", wheres=" + wheres +
                '}';
    }
}

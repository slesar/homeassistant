package nest.model;

import android.support.annotation.NonNull;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

@JsonObject
public abstract class NestDevice extends FirebaseObject {

    public static final String PATH = "devices";

    public static final String FIELD_SOFTWARE_VERSION = "software_version";
    public static final String FIELD_DEVICE_ID = "device_id"; // en-US
    public static final String FIELD_STRUCTURE_ID = "structure_id";
    public static final String FIELD_LOCALE = "locale";

    public static final String FIELD_NAME = "name";
    public static final String FIELD_NAME_LONG = "name_long";

    public static final String FIELD_LAST_CONNECTION = "last_connection"; // "2017-06-20T11:55:52.777Z"
    public static final String FIELD_IS_ONLINE = "is_online";

    public static final String FIELD_WHERE_ID = "where_id";
    public static final String FIELD_WHERE_NAME = "where_name";

    @JsonField(name = FIELD_SOFTWARE_VERSION)
    public String softwareVersion;

    @JsonField(name = FIELD_DEVICE_ID)
    public String deviceId;

    @JsonField(name = FIELD_STRUCTURE_ID)
    public String structureId;

    @JsonField(name = FIELD_LOCALE)
    public String locale;

    @JsonField(name = FIELD_NAME)
    public String name;

    @JsonField(name = FIELD_NAME_LONG)
    public String nameLong;

    @JsonField(name = FIELD_LAST_CONNECTION)
    public String lastConnection;

    @JsonField(name = FIELD_IS_ONLINE)
    public boolean isOnline;

    @JsonField(name = FIELD_WHERE_ID)
    public String whereId;

    @JsonField(name = FIELD_WHERE_NAME)
    public String whereName;

    public NestDevice() {
    }

    @NonNull
    @Override
    public String getPath(@NonNull String deviceId, @NonNull String field) {
        return "/" + PATH + "/" + getDevicePath() + "/" + deviceId + "/" + field;
    }

    protected abstract String getDevicePath();

    @Override
    public String toString() {
        return "Device{" +
                "\ndeviceId='" + deviceId + '\'' +
                ",\nlocale='" + locale + '\'' +
                ",\nsoftwareVersion='" + softwareVersion + '\'' +
                ",\nstructureId='" + structureId + '\'' +
                ",\nname='" + name + '\'' +
                ",\nnameLong='" + nameLong + '\'' +
                ",\nlastConnection='" + lastConnection + '\'' +
                ",\nisOnline=" + isOnline +
                '}';
    }
}

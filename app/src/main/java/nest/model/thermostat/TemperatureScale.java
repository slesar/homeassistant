package nest.model.thermostat;

import android.support.annotation.NonNull;

public enum TemperatureScale {
    F("F"),
    C("C");

    @NonNull
    public static TemperatureScale from(@NonNull String key) {
        for (TemperatureScale value : values()) {
            if (value.key.equalsIgnoreCase(key)) {
                return value;
            }
        }
        return C;
    }

    private final String key;

    TemperatureScale(@NonNull String key) {
        this.key = key;
    }

    @NonNull
    public String getKey() {
        return key;
    }
}

package nest.model.thermostat;

import android.support.annotation.NonNull;

public enum HvacState {
    OFF("off"),
    UNKNOWN("unknown");

    @NonNull
    public static HvacState from(@NonNull String key) {
        for (HvacState value : values()) {
            if (value.key.equalsIgnoreCase(key)) {
                return value;
            }
        }
        return UNKNOWN;
    }

    private final String key;

    HvacState(@NonNull String key) {
        this.key = key;
    }

    @NonNull
    public String getKey() {
        return key;
    }
}

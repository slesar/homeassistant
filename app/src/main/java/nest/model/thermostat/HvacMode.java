package nest.model.thermostat;

import android.support.annotation.NonNull;

public enum HvacMode {

    HEAT("heat"),
    COOL("cool"),
    HEAT_AND_COOL("heat-cool"),
    ECO("eco"),
    OFF("off"),
    UNKNOWN("unknown");

    @NonNull
    public static HvacMode from(@NonNull String key) {
        for (HvacMode value : values()) {
            if (value.key.equalsIgnoreCase(key)) {
                return value;
            }
        }
        return UNKNOWN;
    }

    private final String key;

    HvacMode(@NonNull String key) {
        this.key = key;
    }

    @NonNull
    public String getKey() {
        return key;
    }
}

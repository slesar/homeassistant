package nest.model.thermostat;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import nest.model.NestDevice;
import nest.retrofit.converter.HvacModeConverter;
import nest.retrofit.converter.HvacStateConverter;
import nest.retrofit.converter.TemperatureScaleModeConverter;

@JsonObject
public class NestThermostat extends NestDevice {

    public static final String PATH = "thermostats";

    public static final String FIELD_CAN_COOL = "can_cool";
    public static final String FIELD_CAN_HEAT = "can_heat";

    public static final String FIELD_EMERGENCY_HEAT = "is_using_emergency_heat";
    public static final String FIELD_SUNLIGHT_CORRECTION_ACTIVE = "sunlight_correction_active";
    public static final String FIELD_SUNLIGHT_CORRECTION_ENABLED = "sunlight_correction_enabled";

    public static final String FIELD_HAS_FAN = "has_fan";
    public static final String FIELD_FAN_TIMEOUT = "fan_timer_timeout"; // "1970-01-01T00:00:00.000Z"
    public static final String FIELD_FAN_TIMER_ACTIVE = "fan_timer_active";
    public static final String FIELD_FAN_TIMER_DURATION = "fan_timer_duration";

    public static final String FIELD_HUMIDITY = "humidity";
    public static final String FIELD_HAS_LEAF = "has_leaf";
    public static final String FIELD_TEMP_SCALE = "temperature_scale";

    public static final String FIELD_AWAY_TEMP_HIGH_F = "away_temperature_high_f";
    public static final String FIELD_AWAY_TEMP_HIGH_C = "away_temperature_high_c";
    public static final String FIELD_AWAY_TEMP_LOW_F = "away_temperature_low_f";
    public static final String FIELD_AWAY_TEMP_LOW_C = "away_temperature_low_c";

    public static final String FIELD_AMBIENT_TEMP_F = "ambient_temperature_f";
    public static final String FIELD_AMBIENT_TEMP_C = "ambient_temperature_c";

    public static final String FIELD_ECO_TEMP_HIGH_F = "eco_temperature_high_f";
    public static final String FIELD_ECO_TEMP_HIGH_C = "eco_temperature_high_c";
    public static final String FIELD_ECO_TEMP_LOW_F = "eco_temperature_low_f";
    public static final String FIELD_ECO_TEMP_LOW_C = "eco_temperature_low_c";

    public static final String FIELD_TARGET_TEMP_F = "target_temperature_f";
    public static final String FIELD_TARGET_TEMP_C = "target_temperature_c";
    public static final String FIELD_TARGET_TEMP_HIGH_F = "target_temperature_high_f";
    public static final String FIELD_TARGET_TEMP_HIGH_C = "target_temperature_high_c";
    public static final String FIELD_TARGET_TEMP_LOW_F = "target_temperature_low_f";
    public static final String FIELD_TARGET_TEMP_LOW_C = "target_temperature_low_c";

    public static final String FIELD_HVAC_MODE = "hvac_mode";
    public static final String FIELD_PREVIOUS_HVAC_MODE = "previous_hvac_mode";
    public static final String FIELD_HVAC_STATE = "hvac_state";

    public static final String FIELD_IS_LOCKED = "is_locked";
    public static final String FIELD_LOCKED_TEMP_MAX_F = "locked_temp_max_f";
    public static final String FIELD_LOCKED_TEMP_MAX_C = "locked_temp_max_c";
    public static final String FIELD_LOCKED_TEMP_MIN_F = "locked_temp_min_f";
    public static final String FIELD_LOCKED_TEMP_MIN_C = "locked_temp_min_c";

    public static final String FIELD_TIME_TO_TARGET = "time_to_target";
    public static final String FIELD_TIME_TO_TARGET_TRAINING = "time_to_target_training"; // "ready"

    public static final String FIELD_LABEL = "label";

    @JsonField(name = FIELD_CAN_COOL)
    public boolean canCool;

    @JsonField(name = FIELD_CAN_HEAT)
    public boolean canHeat;

    @JsonField(name = FIELD_EMERGENCY_HEAT)
    public boolean isUsingEmergencyHeat;

    @JsonField(name = FIELD_HAS_FAN)
    public boolean hasFan;

    @JsonField(name = FIELD_HUMIDITY)
    public int humidity;

    @JsonField(name = FIELD_FAN_TIMEOUT)
    public String fanTimerTimeout;

    @JsonField(name = FIELD_HAS_LEAF)
    public boolean hasLeaf;

    @JsonField(name = FIELD_TEMP_SCALE, typeConverter = TemperatureScaleModeConverter.class)
    public TemperatureScale temperatureScale;

    @JsonField(name = FIELD_AWAY_TEMP_HIGH_F)
    public long awayTemperatureHighF;

    @JsonField(name = FIELD_AWAY_TEMP_HIGH_C)
    public double awayTemperatureHighC;

    @JsonField(name = FIELD_AWAY_TEMP_LOW_F)
    public long awayTemperatureLowF;

    @JsonField(name = FIELD_AWAY_TEMP_LOW_C)
    public double awayTemperatureLowC;

    @JsonField(name = FIELD_AMBIENT_TEMP_F)
    public long ambientTemperatureF;

    @JsonField(name = FIELD_AMBIENT_TEMP_C)
    public double ambientTemperatureC;

    @JsonField(name = FIELD_FAN_TIMER_ACTIVE)
    public boolean fanTimerActive;

    @JsonField(name = FIELD_TARGET_TEMP_F)
    public long targetTemperatureF;

    @JsonField(name = FIELD_TARGET_TEMP_C)
    public double targetTemperatureC;

    @JsonField(name = FIELD_TARGET_TEMP_HIGH_F)
    public long targetTemperatureHighF;

    @JsonField(name = FIELD_TARGET_TEMP_HIGH_C)
    public double targetTemperatureHighC;

    @JsonField(name = FIELD_TARGET_TEMP_LOW_F)
    public long targetTemperatureLowF;

    @JsonField(name = FIELD_TARGET_TEMP_LOW_C)
    public double targetTemperatureLowC;

    @JsonField(name = FIELD_HVAC_MODE, typeConverter = HvacModeConverter.class)
    public HvacMode hvacMode;

    @JsonField(name = FIELD_PREVIOUS_HVAC_MODE, typeConverter = HvacModeConverter.class)
    public HvacMode previousHvacMode;

    @JsonField(name = FIELD_HVAC_STATE, typeConverter = HvacStateConverter.class)
    public HvacState hvacState;

    public NestThermostat() {
    }

    @Override
    protected String getDevicePath() {
        return PATH;
    }
}

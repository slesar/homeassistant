package nest.model.account;

import android.support.annotation.NonNull;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import nest.model.FirebaseObject;

@JsonObject
public class MetaData extends FirebaseObject {

    public static final String PATH = "metadata";

    public static final String FIELD_ACCESS_TOKEN = "access_token";
    public static final String FIELD_CLIENT_VERSION = "client_version";

    @JsonField(name = FIELD_ACCESS_TOKEN)
    public String accessToken;

    @JsonField(name = FIELD_CLIENT_VERSION)
    public long clientVersion;

    public MetaData() {
    }

    @NonNull
    @Override
    public String getPath(@NonNull String accessToken, @NonNull String field) {
        // TODO
        return "/" + PATH + "/" + accessToken + "/" + field;
    }

    @Override
    public String toString() {
        return "MetaData{" +
                "accessToken='" + accessToken + '\'' +
                ", clientVersion=" + clientVersion +
                '}';
    }
}

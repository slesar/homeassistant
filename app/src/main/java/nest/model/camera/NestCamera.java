package nest.model.camera;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import nest.model.NestDevice;

@JsonObject
public class NestCamera extends NestDevice {

    public static final String PATH = "cameras";

    public static final String FIELD_APP_URL = "app_url"; // "nestmobile://cameras/CjZp0QQ?auth=c.vNH";
    public static final String FIELD_WEB_URL = "web_url"; // "https://home.nest.com/cameras/CjZQ?auth=c.vUM";
    public static final String FIELD_LAST_IS_ONLINE_CHANGE = "last_is_online_change"; // "2017-06-20T11:45:15.000Z";
    public static final String FIELD_LAST_EVENT = "last_event"; // " size = 8";
    public static final String FIELD_IS_STREAMING = "is_streaming";
    public static final String FIELD_IS_VIDEO_HISTORY_ENABLED = "is_video_history_enabled";
    public static final String FIELD_IS_AUDIO_INPUT_ENABLED = "is_audio_input_enabled";

    @JsonField(name = FIELD_APP_URL)
    public String appUrl;

    @JsonField(name = FIELD_WEB_URL)
    public String webUrl;

    @JsonField(name = FIELD_LAST_IS_ONLINE_CHANGE)
    public String lastIsOnlineChange;

    @JsonField(name = FIELD_LAST_EVENT)
    public String lastEvent;

    @JsonField(name = FIELD_IS_STREAMING)
    public boolean isStreaming;

    @JsonField(name = FIELD_IS_VIDEO_HISTORY_ENABLED)
    public boolean isVideoHistoryEnabled;

    @JsonField(name = FIELD_IS_AUDIO_INPUT_ENABLED)
    public boolean isAudioInputEnabled;

    @Override
    protected String getDevicePath() {
        return PATH;
    }
}

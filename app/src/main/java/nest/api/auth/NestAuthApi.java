package nest.api.auth;

import android.support.annotation.NonNull;

import io.reactivex.Observable;
import assistant.model.EmptyBody;
import nest.model.account.AccessToken;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface NestAuthApi {

    String FIREBASE_NEST_URL = "https://developer-api.nest.com";

    String REQUEST_CLIENT_CODE_URL = "https://home.nest.com/login/oauth2?client_id=%s&state=%s";

    String ACCESS_TOKEN_ENDPOINT = "https://api.home.nest.com/oauth2/";

    /**
     * Use with {@link #ACCESS_TOKEN_ENDPOINT}
     */
    @POST("access_token?grant_type=authorization_code")
    @Headers("Content-type: application/x-www-form-urlencoded; charset=UTF-8")
    Observable<AccessToken> getAccessToken(
            @Query("code") @NonNull String code,
            @Query("client_id") @NonNull String clientId,
            @Query("client_secret") @NonNull String clientSecret,
            @Body @NonNull EmptyBody body
    );
}

package nest.api.auth;

import android.support.annotation.NonNull;

import com.github.aurae.retrofit2.LoganSquareConverterFactory;

import io.reactivex.Observable;
import assistant.model.EmptyBody;
import nest.model.account.AccessToken;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

public class NestAuthApiAdapter implements NestAuthApi {

    private final NestAuthApi authAdapter;

    public NestAuthApiAdapter(@NonNull OkHttpClient client) {
        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ACCESS_TOKEN_ENDPOINT)
                .addConverterFactory(LoganSquareConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .build();

        authAdapter = retrofit.create(NestAuthApi.class);
    }

    @Override
    public Observable<AccessToken> getAccessToken(
            @NonNull String code,
            @NonNull String clientId,
            @NonNull String clientSecret,
            @NonNull EmptyBody body) {
        return authAdapter.getAccessToken(code, clientId, clientSecret, body);
    }
}

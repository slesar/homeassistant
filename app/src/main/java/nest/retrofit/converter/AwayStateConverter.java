package nest.retrofit.converter;

import com.bluelinelabs.logansquare.typeconverters.StringBasedTypeConverter;

import nest.model.structure.AwayState;

public class AwayStateConverter extends StringBasedTypeConverter<AwayState> {

    @Override
    public AwayState getFromString(String string) {
        return AwayState.from(string);
    }

    @Override
    public String convertToString(AwayState state) {
        return state.getKey();
    }
}

package nest.retrofit.converter;

import com.bluelinelabs.logansquare.typeconverters.StringBasedTypeConverter;

import nest.model.thermostat.TemperatureScale;

public class TemperatureScaleModeConverter extends StringBasedTypeConverter<TemperatureScale> {

    @Override
    public TemperatureScale getFromString(String string) {
        return TemperatureScale.from(string);
    }

    @Override
    public String convertToString(TemperatureScale mode) {
        return mode.getKey();
    }
}

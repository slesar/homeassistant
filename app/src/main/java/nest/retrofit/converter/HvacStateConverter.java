package nest.retrofit.converter;

import com.bluelinelabs.logansquare.typeconverters.StringBasedTypeConverter;

import nest.model.thermostat.HvacState;

public class HvacStateConverter extends StringBasedTypeConverter<HvacState> {

    @Override
    public HvacState getFromString(String string) {
        return HvacState.from(string);
    }

    @Override
    public String convertToString(HvacState mode) {
        return mode.getKey();
    }
}

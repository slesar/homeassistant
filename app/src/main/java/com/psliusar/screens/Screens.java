package com.psliusar.screens;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.LinkedList;

public class Screens<S extends Screen> {

    public static final int SCREEN_INVALID = -1;

    private static final String ARGS_CURRENT_SCREEN_ID = "SCREEN_SERVICE_CURRENT_SCREEN_ID";

    private final ScreenHost<S> host;

    private LinkedList<QueueItem> pendingScreens = new LinkedList<>();

    public Screens(@NonNull ScreenHost<S> host) {
        this.host = host;
    }

    @NonNull
    public ScreenHost getScreenHost() {
        return host;
    }

    public void init() {
        final int screenId = host.getDefaultScreenId();
        if (screenId != SCREEN_INVALID) {
            showScreen(screenId, null);
        }
    }

    public void saveInstanceState(@NonNull Bundle state) {
        // TODO save queue
    }

    public void restoreInstanceState(@NonNull Bundle state) {
        // TODO restore queue from Bundle
    }

    public void showScreen(int screenId, @Nullable Bundle arguments) {
        if (host.canChangeScreen()) {
            final S screen = host.getScreen(screenId);
            showScreen(screen, arguments == null ? screen.getDefaultParams() : arguments);
        } else {
            // Put screen in queue
            pendingScreens.offer(new QueueItem(screenId, arguments));
        }
    }

    public int getTargetScreenId() {
        // TODO
        return SCREEN_INVALID;
    }

    public int getCurrentScreenId() {
        final Bundle args = host.getCurrentScreenArguments();
        if (args != null) {
            return args.getInt(ARGS_CURRENT_SCREEN_ID, Screens.SCREEN_INVALID);
        }
        return Screens.SCREEN_INVALID;
    }

    private void showScreen(@NonNull S screen, @Nullable Bundle arguments) {
        if (screen.isUpdateOnTop()
                && screen.getScreenId() == getCurrentScreenId()
                && host.updateScreen(screen, arguments)) {
            return;
        } else if (screen.isClearStack()) {
            host.clearScreens();
        }
        arguments = requireBundle(arguments);
        arguments.putInt(ARGS_CURRENT_SCREEN_ID, screen.getScreenId());
        host.addScreen(screen, arguments);
    }

    public boolean popStack() {
        return host.popScreen();
    }

    public void processQueue() {
        if (!host.canChangeScreen()) {
            return;
        }
        while (pendingScreens.size() > 0) {
            final QueueItem item = pendingScreens.pollFirst();
            showScreen(item.screenId, item.params);
        }
    }

    @NonNull
    private static Bundle requireBundle(@Nullable Bundle bundle) {
        return bundle == null ? new Bundle() : bundle;
    }

    private static class QueueItem {

        final int screenId;
        final Bundle params;

        public QueueItem(int screenId, @Nullable Bundle params) {
            this.screenId = screenId;
            this.params = params;
        }
    }
}

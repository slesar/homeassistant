package com.psliusar.screens;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class Screen<S extends Screen> {

    private final Class<?> screenClass;
    private final int screenId;
    private String name;
    private boolean clearStack;
    private boolean updateOnTop = true;
    private Bundle defaultParams;
    private int parentScreenId = Screens.SCREEN_INVALID;

    public Screen(@NonNull Class<?> screenClass, int screenId) {
        this.screenClass = screenClass;
        this.screenId = screenId;
    }

    @NonNull
    public Class<?> getScreenClass() {
        return screenClass;
    }

    public int getScreenId() {
        return screenId;
    }

    @NonNull
    public S setName(@NonNull String name) {
        this.name = name;
        //noinspection unchecked
        return (S) this;
    }

    @NonNull
    public String getName() {
        return name;
    }

    @NonNull
    public S setClearStack(boolean value) {
        clearStack = value;
        //noinspection unchecked
        return (S) this;
    }

    public boolean isClearStack() {
        return clearStack;
    }

    @NonNull
    public S setUpdateOnTop(boolean value) {
        updateOnTop = value;
        //noinspection unchecked
        return (S) this;
    }

    public boolean isUpdateOnTop() {
        return updateOnTop;
    }

    @NonNull
    public S setDefaultParams(@Nullable Bundle params) {
        defaultParams = params;
        //noinspection unchecked
        return (S) this;
    }

    @Nullable
    public Bundle getDefaultParams() {
        return defaultParams;
    }

    @NonNull
    public S setParentScreenId(int screenId) {
        parentScreenId = screenId;
        //noinspection unchecked
        return (S) this;
    }

    public int getParentScreenId() {
        return parentScreenId;
    }
}

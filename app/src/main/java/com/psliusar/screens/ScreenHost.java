package com.psliusar.screens;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public interface ScreenHost<S extends Screen> {

    @NonNull
    S getScreen(int screenId);

    boolean addScreen(@NonNull S screen, @Nullable Bundle arguments);
    boolean updateScreen(@NonNull S screen, @Nullable Bundle arguments);
    boolean popScreen();

    boolean clearScreens();

    int getDefaultScreenId();
    @Nullable
    Bundle getCurrentScreenArguments();

    boolean canChangeScreen();
}

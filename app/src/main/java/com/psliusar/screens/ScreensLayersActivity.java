package com.psliusar.screens;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.psliusar.layers.Layer;
import com.psliusar.layers.LayersActivity;

import assistant.ui.screen.ScreenRepo;

@Deprecated
public abstract class ScreensLayersActivity<S extends Screen> extends LayersActivity implements ScreenHost<S> {

    private Screens<S> screens;

    public ScreensLayersActivity() {
        screens = new Screens<>(this);
        screens.init();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        screens.saveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle state) {
        super.onRestoreInstanceState(state);
        screens.restoreInstanceState(state);
    }

    @NonNull
    @Override
    public abstract S getScreen(int screenId);

    @Override
    public boolean addScreen(@NonNull S screen, @Nullable Bundle arguments) {
        getLayers()
                .add((Class<Layer>) screen.getScreenClass())
                .setName(screen.getName())
                .setArguments(arguments)
                .commit();
        return true;
    }

    @Override
    public boolean updateScreen(@NonNull S screen, @Nullable Bundle arguments) {
        final Layer<?> topLayer = getLayers().peek();
        if (screen.getScreenClass().isInstance(topLayer)) {
            // TODO update screen
            return true;
        }
        return false;
    }

    @Override
    public boolean popScreen() {
        return getLayers().pop() != null;
    }

    @Override
    public boolean clearScreens() {
        return getLayers().clear() > 0;
    }

    @Override
    public abstract int getDefaultScreenId();

    @Nullable
    @Override
    public Bundle getCurrentScreenArguments() {
        final Layer<?> layer = getLayers().peek();
        return layer == null ? null : layer.getArguments();
    }

    @Override
    public boolean canChangeScreen() {
        // TODO hide keyboard and block
        return !isInSavedState() && !isFinishing();
    }

    public void showScreen(int screenId, @Nullable Bundle params) {
        screens.showScreen(screenId, params);
    }
}

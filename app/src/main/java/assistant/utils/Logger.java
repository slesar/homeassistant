package assistant.utils;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import timber.log.Timber;

public class Logger extends Timber.Tree {

    private static volatile String packageFilter;
    private static volatile boolean useTraces = true;

    public static void setPackageFilter(String packageName) {
        packageFilter = packageName;
    }

    public static void setUseTraces(boolean value) {
        useTraces = value;
    }

    private static final int STACK_INDEX = 5;
    private static final Pattern ANONYMOUS_CLASS = Pattern.compile("(\\$\\d+)+$");
    private static final int MAX_MESSAGE_LENGTH = 4000;
    private static final String TIMBER_PATH = Timber.class.getName();

    private static String append(String message, String str) {
        if (message == null) {
            return str == null ? "" : str;
        } else {
            return str == null ? message : message + " " + str;
        }
    }

    private static boolean isEmpty(@Nullable CharSequence cs) {
        return cs == null || cs.length() == 0;
    }

    private final boolean curious;

    public Logger() {
        curious = false;
    }

    public Logger(boolean curious) {
        this.curious = curious;
    }

    @Nullable
    private static StackTraceElement getSourceElement(@Nullable StackTraceElement[] stackTrace, int start) {
        if (stackTrace == null || stackTrace.length == 0 || stackTrace.length <= start) {
            return null;
        }
        boolean foundTimber = false;
        for (int i = start; i < stackTrace.length; i++) {
            if (stackTrace[i].getClassName().startsWith(TIMBER_PATH)) {
                foundTimber = true;
            } else if (foundTimber) {
                return stackTrace[i];
            }
        }
        return null;
    }

    private static boolean isPassFilter(@Nullable Throwable t, @NonNull String filter) {
        final StackTraceElement[] trace;
        return t != null
                && (trace = t.getStackTrace()) != null
                && trace.length > 0
                && trace[0].getClassName().startsWith(filter);
    }

    /**
     * Break up {@code message} into maximum-length chunks (if needed) and send to either
     * {@link Log#println(int, String, String) Log.println()} or
     * {@link Log#wtf(String, String) Log.wtf()} for logging.
     *
     * {@inheritDoc}
     */
    @Override
    protected void log(int priority, String tag, String message, Throwable t) {
        String source = null;
        if (useTraces || !isEmpty(packageFilter)) {
            final StackTraceElement[] stack = new Throwable().getStackTrace();
            final StackTraceElement trace;
            if (curious) {
                trace = getSourceElement(stack, 2);
            } else if (stack.length <= STACK_INDEX) {
                trace = null;
            } else {
                trace = stack[STACK_INDEX];
            }
            if (trace == null) {
                // no trace
                source = t == null ? "[NO TRACE]" : null;
            } else if (isEmpty(packageFilter)
                    || trace.getClassName().startsWith(packageFilter)
                    || isPassFilter(t, packageFilter)) {
                if (tag == null) {
                    String className = trace.getClassName();
                    final Matcher m = ANONYMOUS_CLASS.matcher(className);
                    if (m.find()) {
                        className = m.replaceAll("");
                    }
                    tag = className.substring(className.lastIndexOf('.') + 1);
                }
                if (t == null) {
                    source = "at " + trace;
                }
            } else {
                // on different package
                return;
            }
        }

        if (message.length() < MAX_MESSAGE_LENGTH) {
            if (priority == Log.ASSERT) {
                Log.wtf(tag, append(message, source));
            } else {
                Log.println(priority, tag, append(message, source));
            }
            return;
        }

        // Split by line, then ensure each line can fit into Log's maximum length.
        for (int i = 0, length = message.length(); i < length; i++) {
            int newline = message.indexOf('\n', i);
            newline = newline != -1 ? newline : length;
            do {
                int end = Math.min(newline, i + MAX_MESSAGE_LENGTH);
                String part = message.substring(i, end);
                if (i == 0) {
                    part = append(part, source);
                }
                if (priority == Log.ASSERT) {
                    Log.wtf(tag, part);
                } else {
                    Log.println(priority, tag, part);
                }
                i = end;
            } while (i < newline);
        }
    }
}

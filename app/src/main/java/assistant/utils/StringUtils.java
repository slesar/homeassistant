package assistant.utils;

import android.support.annotation.Nullable;

public class StringUtils {

    public static boolean isEmpty(@Nullable CharSequence text) {
        return text == null || text.length() < 1;
    }

    public static boolean isNotEmpty(@Nullable CharSequence text) {
        return text != null && text.length() > 0;
    }
}

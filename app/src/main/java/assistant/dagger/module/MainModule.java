package assistant.dagger.module;

import android.content.Context;
import android.support.annotation.NonNull;

import com.firebase.client.Config;
import com.firebase.client.Firebase;
import com.firebase.client.Logger;

import javax.inject.Named;
import javax.inject.Provider;
import javax.inject.Singleton;

import assistant.App;
import assistant.BuildConfig;
import assistant.service.UserPreferencesService;
import assistant.service.account.AccountService;
import assistant.service.api.ApiService;
import assistant.service.data.DataReceiver;
import assistant.service.data.DataService;
import assistant.service.data.receivers.NestDataReceiver;
import nest.service.NestApiService;
import nest.service.NestAuthService;
import dagger.Lazy;
import dagger.Module;
import dagger.Provides;
import nest.api.auth.NestAuthApi;
import nest.api.auth.NestAuthApiAdapter;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import timber.log.Timber;

@Module
public class MainModule {

    private final App app;

    public MainModule(@NonNull App application) {
        app = application;
    }

    @Provides
    App provideApp() {
        return app;
    }

    @Provides
    Context provideContext() {
        return app.getApplicationContext();
    }

    @Provides
    NestAuthApi provideNestAuthApi(OkHttpClient client) {
        return new NestAuthApiAdapter(client);
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient() {
        final HttpLoggingInterceptor logging = new HttpLoggingInterceptor(Timber::v);
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
        return new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();
    }

    @Provides
    Firebase provideFirebase(Context context) {
        Firebase.setAndroidContext(context);
        Firebase.goOffline();
        Firebase.goOnline();
        final Config defaultConfig = Firebase.getDefaultConfig();
        defaultConfig.setAuthenticationServer(NestAuthApi.FIREBASE_NEST_URL);
        defaultConfig.setLogLevel(Logger.Level.DEBUG);
        return new Firebase(NestAuthApi.FIREBASE_NEST_URL);
    }

    @Provides
    UserPreferencesService provideUserPreferencesService() {
        return new UserPreferencesService(app);
    }

    @Provides
    @Singleton
    NestAuthService provideNestAuthService(
            NestAuthApi authApi,
            NestApiService nestApiService,
            Provider<UserPreferencesService> prefs) {
        return new NestAuthService(
                authApi,
                nestApiService,
                prefs,
                BuildConfig.NEST_CLIENT_ID,
                BuildConfig.NEST_CLIENT_SECRET,
                BuildConfig.NEST_REDIRECT_URL);
    }

    @Provides
    @Singleton
    NestApiService provideNestApiService(Lazy<Firebase> firebase, @Named("Nest") DataReceiver dataReceiver) {
        return new NestApiService(firebase, dataReceiver);
    }

    @Provides
    @Named("Nest")
    DataReceiver provideNestDataReceiver(DataService dataService) {
        return new NestDataReceiver(dataService);
    }

    @Provides
    @Singleton
    DataService provideDataService() {
        return new DataService();
    }

    @Provides
    ApiService provideApiService() {
        return new ApiService();
    }

    @Provides
    AccountService provideAccountService(NestAuthService nestAuthService) {
        return new AccountService(nestAuthService);
    }
}

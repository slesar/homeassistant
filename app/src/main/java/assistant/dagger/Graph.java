package assistant.dagger;

import android.support.annotation.NonNull;

import assistant.App;
import assistant.dagger.module.MainModule;
import assistant.service.data.receivers.NestThermostatBackingResource;
import assistant.ui.activity.MainActivity;
import assistant.ui.screen.accounts.AccountsLayer;
import assistant.ui.screen.auth.NestAuthLayer;
import assistant.ui.screen.home.HomeLayer;
import assistant.ui.screen.homes.HomesLayer;
import assistant.ui.screen.thermostat.ThermostatLayer;

public interface Graph {

    // Application
    void inject(App app);

    // Activity
    void inject(MainActivity mainActivity);

    // Layers
    void inject(AccountsLayer layer);
    void inject(NestAuthLayer layer);
    void inject(HomesLayer layer);
    void inject(HomeLayer layer);
    void inject(ThermostatLayer layer);

    // Other
    void inject(NestThermostatBackingResource backingResource);

    final class Initializer {
        private Initializer() {
        } // No instances.

        public static DaggerGraph init(@NonNull App app) {
            final DaggerGraph graph = DaggerDaggerGraph.builder()
                    .mainModule(new MainModule(app))
                    .build();
            graph.inject(app);
            return graph;
        }
    }
}

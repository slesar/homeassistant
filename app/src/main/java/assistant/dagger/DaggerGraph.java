package assistant.dagger;

import javax.inject.Singleton;

import dagger.Component;
import assistant.dagger.module.MainModule;

@Singleton
@Component(modules = {MainModule.class})
public interface DaggerGraph extends Graph {
}

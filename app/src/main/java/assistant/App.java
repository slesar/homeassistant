package assistant;

import android.app.Application;

import assistant.dagger.DaggerGraph;

public class App extends Application {

    private static DaggerGraph daggerGraph;

    public static DaggerGraph getGraph() {
        return daggerGraph;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        daggerGraph = DaggerGraph.Initializer.init(this);

        initLogging();
    }

    void initLogging() {

    }
}

package assistant.service.account;

import android.support.annotation.NonNull;

import nest.service.NestAuthService;

public class AccountService {

    private final NestAuthService nestAuthService;

    public AccountService(@NonNull NestAuthService nestAuthService) {
        this.nestAuthService = nestAuthService;
    }

    public void signIn() {
        // TODO
    }

    public boolean isSignedIn() {
        return nestAuthService.isAuthenticated();
    }

    public boolean isInProgress() {
        return nestAuthService.inProgress();
    }
}

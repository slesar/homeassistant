package assistant.service.api;

import android.support.annotation.NonNull;

import assistant.model.thermostat.Thermostat;
import assistant.model.thermostat.ThermostatBackingResource;
import assistant.service.data.receivers.NestDataReceiver;
import assistant.service.data.receivers.NestThermostatBackingResource;

public class ApiService {

    private final NestThermostatBackingResource nestThermostatBackingResource = new NestThermostatBackingResource();

    public ApiService() {
    }

    @NonNull
    public ThermostatBackingResource getBackingResource(@NonNull String deviceId) {
        if (NestDataReceiver.isNestDevice(deviceId)) {
            return nestThermostatBackingResource;
        }
        throw new IllegalArgumentException("Unable to determine device's producer by ID: " + deviceId);
    }
}

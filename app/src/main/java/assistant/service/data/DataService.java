package assistant.service.data;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import assistant.model.Device;
import assistant.model.camera.Camera;
import assistant.model.home.Home;
import assistant.model.thermostat.Thermostat;
import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;

public class DataService {

    private static final String KEY_HOME = "home";
    private static final String KEY_THERMOSTAT = "thermostat";
    private static final String KEY_CAMERA = "camera";

    private final BehaviorSubject<Home> homeSubject = BehaviorSubject.create();
    private final BehaviorSubject<Thermostat> thermostatSubject = BehaviorSubject.create();
    private final BehaviorSubject<Camera> cameraSubject = BehaviorSubject.create();

    private final HashMap<String, HashMap<String, ?>> data = new HashMap<>();

    public DataService() {

    }

    @NonNull
    public List<Home> getAllHomes() {
        return new ArrayList<>(getHomes().values());
    }

    @Nullable
    public Home getHome(@NonNull String homeId) {
        return getHomes().get(homeId);
    }

    @Nullable
    public Device getDevice(@NonNull String deviceId) {
        final String[] types = { KEY_THERMOSTAT, KEY_CAMERA };
        for (String type : types) {
            final Device device = (Device) ensureMapExists(type).get(deviceId);
            if (device != null) {
                return device;
            }
        }
        return null;
    }

    @Nullable
    public Thermostat getThermostat(@NonNull String deviceId) {
        return getThermostats().get(deviceId);
    }

    @NonNull
    public Observable<Home> getHomeObservable() {
        return homeSubject;
    }

    @NonNull
    public Observable<Thermostat> getThermostatObservable() {
        return thermostatSubject;
    }

    @NonNull
    public Observable<Camera> getCameraObservable() {
        return cameraSubject;
    }

    public synchronized void clear(@NonNull String prefix) {
        data.clear();
    }

    public synchronized void clear() {
        data.clear();
    }

    public synchronized void putHome(@NonNull Home home) {
        getHomes().put(home.getId(), home);
        homeSubject.onNext(home);
    }

    public synchronized void putThermostat(@NonNull Thermostat thermostat) {
        getThermostats().put(thermostat.getId(), thermostat);
        thermostatSubject.onNext(thermostat);
    }

    public synchronized void putCamera(@NonNull Camera camera) {
        getCameras().put(camera.getId(), camera);
        cameraSubject.onNext(camera);
    }

    @NonNull
    private synchronized HashMap<String, Home> getHomes() {
        return ensureMapExists(KEY_HOME);
    }

    @NonNull
    private synchronized HashMap<String, Thermostat> getThermostats() {
        return ensureMapExists(KEY_THERMOSTAT);
    }

    @NonNull
    private synchronized HashMap<String, Camera> getCameras() {
        return ensureMapExists(KEY_CAMERA);
    }

    @NonNull
    private synchronized <T> HashMap<String, T> ensureMapExists(@NonNull String type) {
        HashMap<String, T> map = (HashMap<String, T>) data.get(type);
        if (map == null) {
            map = new HashMap<>();
            data.put(type, map);
        }
        return map;
    }

}

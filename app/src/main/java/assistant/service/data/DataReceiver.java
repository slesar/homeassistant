package assistant.service.data;

import android.support.annotation.NonNull;

public interface DataReceiver {

    void putHome(@NonNull Object original);

    void putThermostat(@NonNull Object original);

    void putCamera(@NonNull Object original);

    void clear();
}

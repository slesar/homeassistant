package assistant.service.data.receivers;

import android.support.annotation.NonNull;

import java.util.ArrayList;

import assistant.model.camera.Camera;
import assistant.model.home.Home;
import assistant.model.home.HomeAwayState;
import assistant.model.thermostat.FanState;
import assistant.model.thermostat.Thermostat;
import assistant.model.thermostat.ThermostatMode;
import assistant.service.data.DataReceiver;
import assistant.service.data.DataService;
import nest.model.camera.NestCamera;
import nest.model.structure.NestStructure;
import nest.model.thermostat.NestThermostat;

public class NestDataReceiver implements DataReceiver {

    private static final String PREFIX = "NEST:";

    public static boolean isNestDevice(@NonNull String deviceId) {
        return deviceId.startsWith(PREFIX);
    }

    @NonNull
    public static String stripPrefix(@NonNull String deviceId) {
        if (deviceId.startsWith(PREFIX)) {
            return deviceId.substring(PREFIX.length());
        }
        return deviceId;
    }

    private final DataService dataService;

    public NestDataReceiver(@NonNull DataService dataService) {
        this.dataService = dataService;
    }

    @Override
    public void putHome(@NonNull Object original) {
        final NestStructure nestStructure = (NestStructure) original;
        final Home home = new Home(PREFIX + nestStructure.structureId);
        home.setName(nestStructure.name);

        final ArrayList<String> deviceIds = new ArrayList<>();
        deviceIds.addAll(nestStructure.thermostatIds);
        for (String deviceId : deviceIds) {
            home.addDeviceId(PREFIX + deviceId);
        }

        final HomeAwayState homeAwayState;
        switch (nestStructure.awayState) {
            case HOME:
                homeAwayState = HomeAwayState.HOME;
                break;
            case AWAY:
            case AUTO_AWAY:
            case UNKNOWN:
            default:
                homeAwayState = HomeAwayState.AWAY;
                break;
        }
        home.setHomeAwayState(homeAwayState);

        dataService.putHome(home);
    }

    @Override
    public void putThermostat(@NonNull Object original) {
        final NestThermostat nestThermostat = (NestThermostat) original;
        final Thermostat thermostat = new Thermostat(PREFIX + nestThermostat.deviceId);
        thermostat.setName(nestThermostat.name);
        thermostat.setCanCool(nestThermostat.canCool);
        thermostat.setCanHeat(nestThermostat.canHeat);
        thermostat.setCanRange(nestThermostat.canHeat && nestThermostat.canCool);
        thermostat.setOnline(nestThermostat.isOnline);
        final double upperTemp;
        final double lowerTemp;
        final ThermostatMode mode;
        switch (nestThermostat.hvacMode) {
            case HEAT:
                lowerTemp = nestThermostat.targetTemperatureC;
                upperTemp = nestThermostat.targetTemperatureC;
                mode = ThermostatMode.HEAT;
                break;
            case COOL:
                lowerTemp = nestThermostat.targetTemperatureC;
                upperTemp = nestThermostat.targetTemperatureC;
                mode = ThermostatMode.COOL;
                break;
            case HEAT_AND_COOL:
                lowerTemp = nestThermostat.targetTemperatureLowC;
                upperTemp = nestThermostat.targetTemperatureHighC;
                mode = ThermostatMode.RANGE;
                break;
            case ECO:
                lowerTemp = nestThermostat.awayTemperatureLowC;
                upperTemp = nestThermostat.awayTemperatureHighC;
                mode = ThermostatMode.ECONOMY;
                break;
            case OFF:
            case UNKNOWN:
            default:
                lowerTemp = nestThermostat.targetTemperatureC;
                upperTemp = nestThermostat.targetTemperatureC;
                mode = ThermostatMode.OFF;
                break;
        }
        thermostat.setLowerTemp(lowerTemp);
        thermostat.setUpperTemp(upperTemp);
        thermostat.setCurrentTemp(nestThermostat.ambientTemperatureC);
        thermostat.setHasFan(nestThermostat.hasFan);
        final FanState fanState;
        if (!nestThermostat.hasFan) {
            fanState = FanState.NONE;
        } else if (nestThermostat.fanTimerActive) { // || nestThermostat.fanTimerTimeout
            fanState = FanState.ON;
        } else {
            fanState = FanState.OFF;
        }
        thermostat.setFanState(fanState);
        thermostat.setMode(mode);
        thermostat.setHumidity(nestThermostat.humidity);
        thermostat.setHasHumidity(nestThermostat.humidity > 0);
        dataService.putThermostat(thermostat);
    }

    @Override
    public void putCamera(@NonNull Object original) {
        final NestCamera nestCamera = (NestCamera) original;
        final Camera camera = new Camera(PREFIX + nestCamera.deviceId);
        dataService.putCamera(camera);
    }

    @Override
    public void clear() {
        dataService.clear(PREFIX);
    }
}

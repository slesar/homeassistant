package assistant.service.data.receivers;

@Deprecated
public enum BackingResourceType {
    NEST_STRUCTURE,
    NEST_THERMOSTAT,
    NEST_CAMERA;

    BackingResourceType() {
    }
}

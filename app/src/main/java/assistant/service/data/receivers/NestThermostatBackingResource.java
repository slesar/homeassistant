package assistant.service.data.receivers;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import assistant.App;
import assistant.model.thermostat.Thermostat;
import assistant.model.thermostat.ThermostatBackingResource;
import assistant.model.thermostat.ThermostatMode;
import assistant.service.data.DataService;
import nest.model.thermostat.HvacMode;
import nest.model.thermostat.NestThermostat;
import nest.service.NestApiService;

public class NestThermostatBackingResource implements ThermostatBackingResource {

    private static double normalizeTemp(double temp) {
        return Math.max(9, Math.min(32, temp));
    }

    @Inject
    DataService dataService;

    @Inject
    NestApiService apiService;

    public NestThermostatBackingResource() {
        App.getGraph().inject(this);
    }

    @Override
    public double setUpperTemp(@NonNull Thermostat thermostat, double value) {
        double temp = normalizeTemp(value);

        final String targetField;
        switch (thermostat.getMode()) {
            case COOL:
            case HEAT:
                targetField = NestThermostat.FIELD_TARGET_TEMP_C;
                break;
            case RANGE:
                targetField = NestThermostat.FIELD_TARGET_TEMP_HIGH_C;
                break;
            case OFF:
            default:
                return temp;
        }
        /*if (Double.compare(thermostat.getCurrentTemp(), temp) == 0) {
            return temp;
        }*/

        dataService.putThermostat(thermostat);

        apiService.sendRequest(new NestThermostat().getPath(getId(thermostat), targetField), temp);

        return temp;
    }

    @Override
    public double setLowerTemp(@NonNull Thermostat thermostat, double value) {
        double temp = normalizeTemp(value);

        final String targetField;
        switch (thermostat.getMode()) {
            case COOL:
            case HEAT:
                targetField = NestThermostat.FIELD_TARGET_TEMP_C;
                break;
            case RANGE:
                targetField = NestThermostat.FIELD_TARGET_TEMP_LOW_C;
                break;
            case OFF:
            default:
                return temp;
        }

        dataService.putThermostat(thermostat);

        apiService.sendRequest(new NestThermostat().getPath(getId(thermostat), targetField), temp);

        return temp;
    }

    @NonNull
    @Override
    public ThermostatMode setMode(@NonNull Thermostat thermostat, @NonNull ThermostatMode mode) {
        final HvacMode target;
        switch (mode) {
            case COOL:
                target = HvacMode.COOL;
                break;
            case HEAT:
                target = HvacMode.HEAT;
                break;
            case RANGE:
                target = HvacMode.HEAT_AND_COOL;
                break;
            case OFF:
                target = HvacMode.OFF;
                break;
            default:
                throw new IllegalArgumentException("Unsupported mode: " + mode);
        }

        dataService.putThermostat(thermostat);

        apiService.sendRequest(new NestThermostat().getPath(getId(thermostat), NestThermostat.FIELD_HVAC_MODE), target.getKey());

        return mode;
    }

    @NonNull
    private String getId(@NonNull Thermostat thermostat) {
        return NestDataReceiver.stripPrefix(thermostat.getId());
    }
}

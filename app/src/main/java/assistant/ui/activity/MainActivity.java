package assistant.ui.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.ViewGroup;

import com.psliusar.layers.Layer;
import com.psliusar.layers.LayersActivity;
import com.psliusar.screens.ScreenHost;
import com.psliusar.screens.Screens;

import javax.inject.Inject;

import assistant.App;
import assistant.R;
import assistant.service.account.AccountService;
import assistant.ui.screen.ScreenRepo;
import assistant.ui.screen.ScreenRepo.LayerScreen;
import assistant.ui.screen.menu.DrawerMenu;
import assistant.ui.screen.menu.MenuLayer;
import nest.service.NestAuthService;

public class MainActivity extends LayersActivity implements DrawerMenu.MenuListener {

    @Inject
    NestAuthService nestAuthService;

    @Inject
    AccountService accountService;

    private boolean fromSavedState = false;

    /*public interface ArgumentsFactory<T> {

        T getFactory();
    }

    public static class NestAuthArgumentFactory implements ArgumentsFactory<PrepareLayerArguments> {

        @Override
        public PrepareLayerArguments getFactory() {
            return new PrepareLayerArguments();
        }
    }

    public static class PrepareLayerArguments {

        public Bundle requestUrl(@NonNull String requestUrl) {
            return NestAuthLayer.requestUrlArguments(requestUrl);
        }
    }

    public interface CreateFragmentArguments {

        Bundle simple();
        Bundle userInfoArguments(@NonNull String userId);
    }*/

    private Screens<LayerScreen> screens;
    private ScreenHost<LayerScreen> screenHost;

    private DrawerMenu drawerMenu;

    @Override
    protected void onCreate(@Nullable Bundle state) {
        fromSavedState = state != null;
        super.onCreate(state);
        getWindow().setBackgroundDrawableResource(R.color.windowBackground);

        App.getGraph().inject(this);

        setContentView(R.layout.activity_main);

        //nestAuthService.addListener(nestAuthListener);

        if (state == null) {
            getLayers(R.id.menu_container).add(MenuLayer.class).setName("Menu").commit();
        }
        drawerMenu = new DrawerMenu();
        drawerMenu.setMenuListener(this);
        drawerMenu.init(this, R.id.drawer);

        screenHost = new MainActivityScreenHost(this);
        screens = new Screens<>(screenHost);
        screens.init();

        /*if (!nestAuthService.isAuthenticated()) {
            showScreen(ScreenRepo.SCREEN_SPLASH, null);
            nestAuthService.logIn();
        }*/
        nestAuthService.logIn();
        accountService.signIn();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        screens.saveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle state) {
        super.onRestoreInstanceState(state);
        screens.restoreInstanceState(state);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //nestAuthService.removeListener(nestAuthListener);
    }

    @Override
    public void onBackPressed() {
        if (drawerMenu.isOpen()) {
            drawerMenu.close();
            return;
        }
        super.onBackPressed();
    }

    public int getDefaultScreenId() {
        if (isFromSavedState()) {
            return Screens.SCREEN_INVALID;
        }

        return ScreenRepo.SCREEN_HOMES;
        /*if (nestAuthService.isAuthenticated()) {
            // show main screen
            return ScreenRepo.SCREEN_HOMES;
        } else {
            // show sign in
            return ScreenRepo.SCREEN_SPLASH;
        }*/
    }

    @NonNull
    @Override
    public ViewGroup getDefaultContainer() {
        return getView(R.id.root_container);
    }

    @NonNull
    public App getApp() {
        return (App) getApplication();
    }

    public boolean isActivityResumed() {
        return !isInSavedState() && !isFinishing();
    }

    public boolean isFromSavedState() {
        return fromSavedState;
    }

    @Override
    public void onMenuOpened(@NonNull DrawerMenu menu) {

    }

    @Override
    public void onMenuClosed(@NonNull DrawerMenu menu) {
        getLayers().pauseView();
        screens.processQueue();
        getLayers().resumeView();
    }

    public void openMenu() {
        if (!drawerMenu.isOpen()) {
            drawerMenu.open();
        }
    }

    public void closeMenu() {
        if (drawerMenu.isOpen()) {
            drawerMenu.close();
        }
    }

    public void setMenuEnabled(boolean value) {
        drawerMenu.setEnabled(value);
    }

    public void showScreen(int screenId, @Nullable Bundle params) {
        screens.showScreen(screenId, params);
    }

    public static class MainActivityScreenHost implements ScreenHost<LayerScreen> {

        private final MainActivity mainActivity;
        private final ScreenRepo screenRepo;

        public MainActivityScreenHost(@NonNull MainActivity activity) {
            mainActivity = activity;
            screenRepo = new ScreenRepo();
        }

        @NonNull
        @Override
        public LayerScreen getScreen(int screenId) {
            return screenRepo.createScreen(screenId);
        }

        @Override
        public boolean addScreen(@NonNull LayerScreen screen, @Nullable Bundle arguments) {
            mainActivity.getLayers()
                    .add((Class<Layer>) screen.getScreenClass())
                    .setName(screen.getName())
                    .setArguments(arguments)
                    .commit();
            return true;
        }

        @Override
        public boolean updateScreen(@NonNull LayerScreen screen, @Nullable Bundle arguments) {
            final Layer<?> topLayer = mainActivity.getLayers().peek();
            if (screen.getScreenClass().isInstance(topLayer)) {
                // TODO update screen
                return true;
            }
            return false;
        }

        @Override
        public boolean popScreen() {
            return mainActivity.getLayers().pop() != null;
        }

        @Override
        public boolean clearScreens() {
            return mainActivity.getLayers().clear() > 0;
        }

        @Override
        public int getDefaultScreenId() {
            return mainActivity.getDefaultScreenId();
        }

        @Override
        public Bundle getCurrentScreenArguments() {
            final Layer<?> layer = mainActivity.getLayers().peek();
            return layer == null ? null : layer.getArguments();
        }

        @Override
        public boolean canChangeScreen() {
            if (mainActivity.drawerMenu.isOpen()) {
                mainActivity.closeMenu();
            }

            // TODO hide keyboard and block
            return !mainActivity.isInSavedState() && !mainActivity.isFinishing();
        }
    }
}

package assistant.ui.adapter;

import android.support.annotation.NonNull;
import android.view.ViewGroup;

public interface ViewHolderFactory<VH extends BaseViewHolder<T>, T> {

    @NonNull
    VH createViewHolder(@NonNull ViewGroup parent, int viewType);
}

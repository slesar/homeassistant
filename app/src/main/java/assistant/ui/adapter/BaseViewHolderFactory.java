package assistant.ui.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public abstract class BaseViewHolderFactory<VH extends BaseViewHolder<T>, T> implements ViewHolderFactory<VH, T> {

    private final LayoutInflater inflater;

    public BaseViewHolderFactory(@NonNull Context context) {
        inflater = LayoutInflater.from(context);
    }

    @NonNull
    protected View inflate(@LayoutRes int layoutRes, @NonNull ViewGroup parent) {
        return inflater.inflate(layoutRes, parent, false);
    }
}

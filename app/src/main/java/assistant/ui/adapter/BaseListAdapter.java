package assistant.ui.adapter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.List;

public class BaseListAdapter<VH extends BaseViewHolder<T>, T> extends RecyclerView.Adapter<VH> {

    private final ViewHolderFactory<VH, T> viewHolderFactory;

    private List<T> items;

    private AdapterClickListener<VH> clickListener;

    public BaseListAdapter(@NonNull ViewHolderFactory<VH, T> factory) {
        viewHolderFactory = factory;
    }

    @Override
    public VH onCreateViewHolder(ViewGroup parent, int viewType) {
        return viewHolderFactory.createViewHolder(parent, viewType);
    }

    @Override
    public void onBindViewHolder(VH holder, int position) {
        holder.bind(getItem(position));
        if (isItemClickable(position)) {
            holder.itemView.setOnClickListener(v -> clickListener.onAdapterItemClick(holder));
        } else {
            holder.itemView.setOnClickListener(null);
        }
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    @Nullable
    public T getItem(int position) {
        return items == null || position >= items.size() ? null : items.get(position);
    }

    public boolean isItemClickable(int position) {
        return true;
    }

    public void setItems(@Nullable List<T> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    @Nullable
    public List<T> getItems() {
        return items;
    }

    public void setOnClickListener(@Nullable AdapterClickListener<VH> listener) {
        clickListener = listener;
        notifyDataSetChanged();
    }
}

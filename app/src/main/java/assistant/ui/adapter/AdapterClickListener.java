package assistant.ui.adapter;

import android.support.annotation.NonNull;

public interface AdapterClickListener<VH extends BaseViewHolder<?>> {

    void onAdapterItemClick(@NonNull VH viewHolder);
}

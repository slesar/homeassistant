package assistant.ui.adapter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class BaseViewHolder<T> extends RecyclerView.ViewHolder implements View.OnClickListener {

    private T data;

    public BaseViewHolder(@NonNull View itemView) {
        super(itemView);
    }

    @Override
    public void onClick(View v) {

    }

    public void bind(@Nullable T data) {
        this.data = data;
    }

    @Nullable
    public T getData() {
        return data;
    }
}

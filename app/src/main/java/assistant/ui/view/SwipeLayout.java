package assistant.ui.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.Transformation;
import android.widget.FrameLayout;

public class SwipeLayout extends FrameLayout {

    public enum OffsetType {
        BOTTOM_VIEW,
        ABSOLUTE
    }

    public enum SwipeDirection {
        LEFT,
        TOP,
        RIGHT,
        BOTTOM
    }

    public enum BottomViewPosition {
        FIXED,
        MOVABLE,
        CENTERED
    }

    private static final int DURATION_OPEN = 600;
    private static final int DURATION_CLOSE = 400;

    private float swipeVelocity = 0;

    private boolean scrolledByX;
    private boolean scrolledByY;
    private boolean inAnimation = false;

    private int swipeOffset = 0;
    private float maxOffset = 0;
    private OffsetType offsetType = OffsetType.BOTTOM_VIEW;

    private SwipeDirection swipeDirection = SwipeDirection.RIGHT;

    private BottomViewPosition bottomViewPosition = BottomViewPosition.MOVABLE;

    private View bottomView;
    private View topView;

    private int contentWidth;
    private int contentHeight;

    private Drawable shadeDrawable;

    private final Animation.AnimationListener animationListener = new Animation.AnimationListener() {

        @Override
        public void onAnimationStart(Animation animation) {

        }

        @Override
        public void onAnimationEnd(Animation animation) {
            inAnimation = false;
            scrolledByX = false;
            scrolledByY = false;

            if (swipeOffset == 0) {
                bottomView.setVisibility(View.GONE);
            }

            if (listener != null) {
                if (swipeOffset == 0) {
                    listener.onSwipeClose();
                } else {
                    listener.onSwipeOpen();
                }
            }
        }

        @Override
        public void onAnimationRepeat(Animation animation) {

        }
    };

    private final GestureDetector gestureDetector;

    private OnSwipeMoveListener listener;

    public SwipeLayout(Context context) {
        this(context, null);
    }

    public SwipeLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SwipeLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        gestureDetector = new GestureDetector(getContext(), new SwipeGestureListener());
        init(attrs);
    }

    private void init(AttributeSet attrs) {

        setWillNotDraw(false);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);

        contentWidth = right-left - getPaddingLeft() - getPaddingRight();
        contentHeight = bottom-top - getPaddingTop() - getPaddingBottom();

        if (swipeOffset != 0) {
            updateViews();
        } else if (bottomView.getVisibility() == View.VISIBLE && !isInEditMode()) {
            bottomView.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        if (getChildCount() < 2) {
            throw new IllegalStateException("SwipeLayout must have at least two child views");
        }

        if (!isInEditMode()) {
            bottomView = getChildAt(0);
            bottomView.setVisibility(View.GONE);

            topView = getChildAt(1);
            topView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onTouchEvent(@NonNull MotionEvent event) {
        final boolean gesture = gestureDetector.onTouchEvent(event);

        final int action = event.getAction();

        // on up event
        if ((scrolledByX || scrolledByY) && (action == MotionEvent.ACTION_UP || action == MotionEvent.ACTION_CANCEL)) {
            int ofs = getMaxAvailableOffset();

            if (swipeOffset != 0 || swipeOffset != ofs) {
                if (Math.abs(swipeOffset) > Math.abs(ofs) / 2) {
                    // open
                    slide(ofs);
                } else {
                    // close
                    slide(0);
                }
            }
        }

        // dispatch event normally
        if (action == MotionEvent.ACTION_DOWN || (!gesture && !scrolledByX && !scrolledByY && action == MotionEvent.ACTION_UP)) {
            super.onTouchEvent(event);
        }

        return gesture;
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);

        if (shadeDrawable != null && swipeOffset != 0) {
            switch (swipeDirection) {
                case LEFT:
                    shadeDrawable.setBounds(0, 0, swipeOffset, getHeight());
                    break;
                case TOP:
                    shadeDrawable.setBounds(0, 0, getWidth(), swipeOffset);
                    break;
                case RIGHT:
                    shadeDrawable.setBounds(getWidth() + swipeOffset, 0, getWidth(), getHeight());
                    break;
                case BOTTOM:
                    shadeDrawable.setBounds(0, getHeight() + swipeOffset, getWidth(), getHeight());
                    break;
            }

            final float ratio = swipeOffset / (float) getMaxAvailableOffset();
            shadeDrawable.setAlpha(255 - (50 + (int) (205 * ratio * ratio * ratio)));
            shadeDrawable.draw(canvas);
        }
    }

    public void setShadeDrawable(@Nullable Drawable drawable) {
        shadeDrawable = drawable;
    }

    public void setOnSwipeMoveListener(@Nullable OnSwipeMoveListener l) {
        listener = l;
    }

    public void setMaxOffset(float offset) {
        maxOffset = offset;
        // TODO offsetType?
    }

    public void setOffsetType(@NonNull OffsetType type) {
        offsetType = type;
    }

    public void setSwipeDirection(@NonNull SwipeDirection direction) {
        swipeDirection = direction;
    }

    public void setBottomViewPosition(@NonNull BottomViewPosition position) {
        bottomViewPosition = position;
    }

    public void swipeOpen(boolean animated) {
        inAnimation = false;

        if (animated) {
            slide(getMaxAvailableOffset());
        } else {
            swipeOffset = getMaxAvailableOffset();
            requestLayout();
        }
    }

    public void swipeClose(boolean animated) {
        inAnimation = false;

        if (animated) {
            slide(0);
        } else {
            swipeOffset = 0;
            requestLayout();
        }
    }

    private void slide(int offset) {
        if (inAnimation) {
            return;
        }

        int duration = offset == 0 ? DURATION_CLOSE : DURATION_OPEN;
        duration = Math.abs(swipeOffset - offset) * duration / getMaxAvailableOffset();

        if (swipeVelocity > 0) {
            duration = (int) (duration * 1000 / swipeVelocity);
        }

        duration = Math.abs(duration);

        final Animation anim = getSwipeAnimation(offset, duration);

        startAnimation(anim);

        inAnimation = true;
    }

    private Animation getSwipeAnimation(int offset, int duration) {
        SwipeAnimation anim = new SwipeAnimation(offset);
        anim.setDuration(duration);
        anim.setInterpolator(new DecelerateInterpolator());
        anim.setAnimationListener(animationListener);

        return anim;
    }

    private void updateViews() {
        if (bottomView.getVisibility() == View.GONE && !isInEditMode()) {
            bottomView.setVisibility(View.VISIBLE);
        }

        int l = getPaddingLeft();
        int t = getPaddingTop();
        int r = getRight() - getPaddingRight();
        int b = getHeight() - getPaddingBottom();

        switch (swipeDirection) {
            case LEFT:
            case RIGHT:
                topView.setTranslationX(swipeOffset);
                topView.setTranslationY(0);
                break;
            case TOP:
            case BOTTOM:
                topView.setTranslationX(0);
                topView.setTranslationY(swipeOffset);
                break;
        }

        switch (bottomViewPosition) {
            case FIXED:
                // Nothing
                break;
            case MOVABLE:
                switch (swipeDirection) {
                    case LEFT:
                        bottomView.setTranslationX(swipeOffset - bottomView.getWidth());
                        bottomView.setTranslationY(0);
                        break;
                    case TOP:
                        bottomView.setTranslationX(0);
                        bottomView.setTranslationY(swipeOffset - bottomView.getHeight());
                        break;
                    case RIGHT:
                        bottomView.setTranslationX(swipeOffset + bottomView.getWidth());
                        bottomView.setTranslationY(0);
                        break;
                    case BOTTOM:
                        bottomView.setTranslationX(0);
                        bottomView.setTranslationY(swipeOffset + bottomView.getHeight());
                        break;
                }
                break;
            case CENTERED:
                switch (swipeDirection) {
                    case LEFT:
                        bottomView.setTranslationX((swipeOffset - bottomView.getWidth()) / 2);
                        bottomView.setTranslationY(0);
                        break;
                    case TOP:
                        bottomView.setTranslationX(0);
                        bottomView.setTranslationY((swipeOffset - bottomView.getHeight()) / 2);
                        break;
                    case RIGHT:
                        bottomView.setTranslationX((swipeOffset + bottomView.getWidth()) / 2);
                        bottomView.setTranslationY(0);
                        break;
                    case BOTTOM:
                        bottomView.setTranslationX(0);
                        bottomView.setTranslationY((swipeOffset + bottomView.getHeight()) / 2);
                        break;
                }
                break;
        }

        invalidate();
    }

    private int getMaxAvailableOffset() {
        if (offsetType == OffsetType.BOTTOM_VIEW) {
            switch (swipeDirection) {
                case LEFT:
                    return bottomView.getWidth();
                case TOP:
                    return bottomView.getHeight();
                case RIGHT:
                    return -bottomView.getWidth();
                case BOTTOM:
                    return -bottomView.getHeight();
                default:
                    throw new IllegalArgumentException("Unhandled direction type: " + swipeDirection);
            }
        } else if (maxOffset <= 1) {
            // Treat as percents
            switch (swipeDirection) {
                case LEFT:
                    return (int) (contentWidth * maxOffset);
                case TOP:
                    return (int) (contentHeight * maxOffset);
                case RIGHT:
                    return (int) (- contentWidth * maxOffset);
                case BOTTOM:
                    return (int) (- contentHeight * maxOffset);
                default:
                    throw new IllegalArgumentException("Unhandled direction type: " + swipeDirection);
            }
        } else {
            return (int) (maxOffset);
        }
    }

    private int getOffset(float offset) {
        switch (swipeDirection) {
            case LEFT:
            case TOP:
                return (int) Math.min(Math.max(0, offset), getMaxAvailableOffset());
            case RIGHT:
            case BOTTOM:
            default:
                return (int) Math.max(Math.min(0, offset), getMaxAvailableOffset());
        }
    }

    private class SwipeAnimation extends Animation {

        private final int fromOffset;
        private final int toOffset;

        public SwipeAnimation(int to) {
            fromOffset = swipeOffset;
            toOffset = to;
        }

        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t) {
            swipeOffset = (int) (fromOffset + (toOffset - fromOffset) * interpolatedTime);

            if (listener != null) {
                listener.onSwipeMoveAction(swipeOffset / (float) getMaxAvailableOffset());
            }

            updateViews();
        }
    }

    private class SwipeGestureListener extends GestureDetector.SimpleOnGestureListener {

        private static final int DIRECTION_NONE        = 0;
        private static final int DIRECTION_HORIZONTAL  = 1;
        private static final int DIRECTION_VERTICAL    = 2;

        private int direction;

        private int minimumFlingVelocity;

        private int lastMotionX;
        private int lastMotionY;

        private SwipeGestureListener() {
            ViewConfiguration config = ViewConfiguration.get(getContext());

            minimumFlingVelocity = config.getScaledMinimumFlingVelocity() * 20;
        }

        @Override
        public boolean onDown(MotionEvent e) {
            scrolledByX = false;
            scrolledByY = false;

            direction = DIRECTION_NONE;

            lastMotionX = (int) (e.getX() - swipeOffset);
            lastMotionY = (int) (e.getY() - swipeOffset);

            if (inAnimation) {
                clearAnimation();
                inAnimation = false;
            }

            return true;
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            if (direction == DIRECTION_NONE) {
                if (Math.abs(distanceX) > Math.abs(distanceY)) {
                    direction = DIRECTION_HORIZONTAL;
                } else {
                    direction = DIRECTION_VERTICAL;
                }
            }

            if (direction == DIRECTION_HORIZONTAL && (swipeDirection == SwipeDirection.LEFT || swipeDirection == SwipeDirection.RIGHT)) {
                scrolledByX = true;
            } else if (direction == DIRECTION_VERTICAL && (swipeDirection == SwipeDirection.TOP || swipeDirection == SwipeDirection.BOTTOM)) {
                scrolledByY = true;
            }

            if (scrolledByX) {
                final float dx = e2.getX() - lastMotionX;
                swipeOffset = getOffset(dx);
            } else if (scrolledByY) {
                final float dy = e2.getY() - lastMotionY;
                swipeOffset = getOffset(dy);
            } else {
                return false;
            }

            requestDisallowInterceptTouchEvent(true);

            updateViews();

            if (listener != null) {
                listener.onSwipeMoveAction(swipeOffset / (float) getMaxAvailableOffset());
            }

            return true;
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            if (processFling(scrolledByX, velocityX, swipeDirection == SwipeDirection.LEFT)) {
                return true;
            }

            if (processFling(scrolledByY, velocityY, swipeDirection == SwipeDirection.TOP)) {
                return true;
            }

            return false;
        }

         private boolean processFling(boolean scrolled, float velocity, boolean isPositive) {
             if (scrolled) {
                 float absVelocity = Math.abs(velocity);

                 if (absVelocity > minimumFlingVelocity) {
                     swipeVelocity = absVelocity;

                     if (velocity > 0 == isPositive) {
                         slide(getMaxAvailableOffset());
                     } else {
                         slide(0);
                     }

                     swipeVelocity = 0;

                     return true;
                 }
             }
             return false;
         }
    }

    public interface OnSwipeMoveListener {

        void onSwipeMoveAction(float openFraction);
        void onSwipeOpen();
        void onSwipeClose();
    }
}

package assistant.ui.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.widget.TextView;

import assistant.R;

public class Header extends RelativeLayout {

    private TextView titleView;

    public Header(Context context) {
        super(context);
        init();
    }

    public Header(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public Header(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public Header(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.header, this);

        titleView = (TextView) findViewById(R.id.header_title);
    }

    public void setTitle(@StringRes int textRes) {
        titleView.setText(textRes);
    }

    public void setTitle(@Nullable CharSequence text) {
        titleView.setText(text);
    }
}

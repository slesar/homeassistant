package assistant.ui.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import assistant.model.thermostat.Thermostat;

public class ThermostatIconic extends RelativeLayout {

    public ThermostatIconic(Context context) {
        super(context);
    }

    public ThermostatIconic(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ThermostatIconic(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ThermostatIconic(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void setThermostat(@Nullable Thermostat thermostat) {

    }
}

package assistant.ui.screen.auth;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.psliusar.layers.binder.Bind;

import javax.inject.Inject;

import assistant.App;
import assistant.R;
import nest.service.NestAuthService;
import assistant.ui.screen.base.BaseLayer;

public class NestAuthLayer extends BaseLayer<NestAuthPresenter> {

    private static final String ARGS_REQUEST_URL = "ARGS_REQUEST_URL";

    @NonNull
    public static Bundle requestUrlArguments(@NonNull String requestUrl) {
        final Bundle bundle = new Bundle();
        bundle.putString(ARGS_REQUEST_URL, requestUrl);
        return bundle;
    }

    @Bind(R.id.progress)
    ProgressBar progressBar;

    @Bind(R.id.webview)
    WebView webView;

    @Inject
    NestAuthService nestAuthService;

    @Override
    protected NestAuthPresenter onCreatePresenter() {
        return new NestAuthPresenter(nestAuthService);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedState) {
        super.onCreate(savedState);
        App.getGraph().inject(this);
    }

    @Nullable
    @Override
    protected View onCreateView(@Nullable ViewGroup viewGroup) {
        return inflate(R.layout.screen_nest_auth, viewGroup);
    }

    @Override
    protected void onBindView(@NonNull View view) {
        super.onBindView(view);
        showLoginPage();
    }

    private void showLoginPage() {
        hideProgress();

        webView.setWebChromeClient(new ProgressChromeClient());
        webView.setWebViewClient(new RedirectClient());
        webView.getSettings().setJavaScriptEnabled(true);

        webView.loadUrl(getArguments().getString(ARGS_REQUEST_URL));
    }

    private void showProgress() {
        webView.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgress() {
        webView.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
    }

    private class ProgressChromeClient extends WebChromeClient {

        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            if (progressBar != null) {
                final int vis = progressBar.getVisibility();
                if (newProgress < 100 && vis != View.VISIBLE) {
                    showProgress();
                } else if (newProgress == 100 && vis != View.GONE) {
                    hideProgress();
                }
            }
        }
    }

    private class RedirectClient extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (getPresenter().parseRedirectUrl(url)) {
                showProgress();
                return true;
            } else {
                return false;
            }
        }
    }
}

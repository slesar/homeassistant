package assistant.ui.screen.auth;

import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;

import com.psliusar.layers.Model;

import assistant.ui.screen.base.BasePresenter;
import nest.service.NestAuthService;
import timber.log.Timber;

public class NestAuthPresenter extends BasePresenter<Model, NestAuthLayer> {

    protected static void clearCookies(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            Timber.d("Using clearCookies code for API >=" + String.valueOf(Build.VERSION_CODES.LOLLIPOP_MR1));
            CookieManager.getInstance().removeAllCookies(null);
            CookieManager.getInstance().flush();
        } else {
            Timber.d("Using clearCookies code for API <" + String.valueOf(Build.VERSION_CODES.LOLLIPOP_MR1));
            CookieSyncManager cookieSyncMngr=CookieSyncManager.createInstance(context);
            cookieSyncMngr.startSync();
            CookieManager cookieManager=CookieManager.getInstance();
            cookieManager.removeAllCookie();
            cookieManager.removeSessionCookie();
            cookieSyncMngr.stopSync();
            cookieSyncMngr.sync();
        }
    }

    private final NestAuthService nestAuthService;

    private final NestAuthService.NestAuthListener nestAuthListener = new NestAuthService.NestAuthListener() {
        @Override
        public void onRequestAuthCode(@NonNull String requestUrl) {

        }

        @Override
        public void onAuthStateChanged(boolean authenticated) {
            if (authenticated) {
                getLayer().getActivity().onBackPressed();
            } else {
                // Clear WebView cache
                //clearCookies(getApplicationContext());
            }
        }
    };


    public NestAuthPresenter(@NonNull NestAuthService nestAuthService) {
        this.nestAuthService = nestAuthService;
    }

    @Override
    public void start() {
        super.start();
        nestAuthService.addListener(nestAuthListener);
    }

    @Override
    public void stop() {
        super.stop();
        nestAuthService.removeListener(nestAuthListener);
    }

    boolean parseRedirectUrl(@NonNull String url) {
        return nestAuthService.parseRequestCodeRedirectUrl(url);
    }
}

package assistant.ui.screen.homes;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.psliusar.layers.binder.Bind;
import com.psliusar.layers.binder.Binder;

import java.util.List;

import javax.inject.Inject;

import assistant.App;
import assistant.R;
import assistant.model.home.Home;
import assistant.service.account.AccountService;
import assistant.service.data.DataService;
import assistant.ui.adapter.BaseListAdapter;
import assistant.ui.adapter.BaseViewHolder;
import assistant.ui.adapter.BaseViewHolderFactory;
import assistant.ui.screen.base.BaseLayer;
import assistant.ui.view.Header;

public class HomesLayer extends BaseLayer<HomesPresenter> {

    @Bind(R.id.header) Header header;
    @Bind(R.id.homes_list) RecyclerView homesList;
    @Bind(R.id.homes_invitation) View invitation;
    @Bind(value = R.id.homes_accounts_button, clicks = true) View accountsButton;

    @Inject
    DataService dataService;

    @Inject
    AccountService accountService;

    HomesAdapter adapter;

    public HomesLayer() {
        App.getGraph().inject(this);
    }

    @Override
    protected HomesPresenter onCreatePresenter() {
        return new HomesPresenter(dataService, accountService);
    }

    @Nullable
    @Override
    protected View onCreateView(@Nullable ViewGroup viewGroup) {
        return inflate(R.layout.screen_homes, viewGroup);
    }

    @Override
    protected void onBindView(@NonNull View view) {
        super.onBindView(view);

        adapter = new HomesAdapter(getContext());
        adapter.setOnClickListener(viewHolder -> getPresenter().showHome(viewHolder.getData().getId()));

        homesList.setLayoutManager(new LinearLayoutManager(getContext()));
        homesList.setAdapter(adapter);
        homesList.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));

        getPresenter().updateHomesList();
    }

    @Override
    protected void onDestroyView() {
        super.onDestroyView();
        adapter = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.homes_accounts_button:
                getPresenter().showAccounts();
                break;
            default:
                super.onClick(v);
        }
    }

    void showHomes(@NonNull List<Home> homes) {
        homesList.setVisibility(View.VISIBLE);
        invitation.setVisibility(View.GONE);
        adapter.setItems(homes);
    }

    void showInvitation() {
        homesList.setVisibility(View.GONE);
        invitation.setVisibility(View.VISIBLE);
    }

    protected static class HomesAdapter extends BaseListAdapter<HomeViewHolder, Home> {

        public HomesAdapter(@NonNull Context context) {
            super(new HomeViewHolderFactory(context));
        }
    }

    private static class HomeViewHolderFactory extends BaseViewHolderFactory<HomeViewHolder, Home> {

        public HomeViewHolderFactory(@NonNull Context context) {
            super(context);
        }

        @NonNull
        @Override
        public HomeViewHolder createViewHolder(@NonNull ViewGroup parent, int viewType) {
            final View view = inflate(R.layout.homes_home_list_item, parent);
            return new HomeViewHolder(view);
        }
    }

    static class HomeViewHolder extends BaseViewHolder<Home> {

        @Bind(R.id.title)
        TextView title;

        public HomeViewHolder(@NonNull View itemView) {
            super(itemView);
            Binder.bind(this, itemView);
        }

        public void bind(@Nullable Home home) {
            super.bind(home);
            title.setText(home.getName());
        }
    }
}

package assistant.ui.screen.homes;

import android.support.annotation.NonNull;

import assistant.service.account.AccountService;
import assistant.service.data.DataService;
import assistant.ui.screen.ScreenRepo;
import assistant.ui.screen.base.BasePresenter;
import assistant.ui.screen.home.HomeLayer;
import io.reactivex.android.schedulers.AndroidSchedulers;

public class HomesPresenter extends BasePresenter<HomesModel, HomesLayer> {

    private final DataService dataService;
    private final AccountService accountService;

    public HomesPresenter(@NonNull DataService dataService, @NonNull AccountService accountService) {
        this.dataService = dataService;
        this.accountService = accountService;
    }

    @Override
    protected HomesModel onCreateModel() {
        return new HomesModel(dataService, accountService);
    }

    @Override
    public void start() {
        super.start();
        getLayer().header.setTitle("Homes");

        manage(getModel().getHomeObservable()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(home -> updateHomesList()));
    }

    void showAccounts() {
        showScreen(ScreenRepo.SCREEN_ACCOUNTS, null);
    }

    void updateHomesList() {
        if (getLayer().adapter == null) {
            return;
        }

        if (getModel().isSignedIn()) {
            getLayer().showHomes(getModel().getHomes());
        } else {
            getLayer().showInvitation();
        }
    }

    void showHome(@NonNull String homeId) {
        showScreen(ScreenRepo.SCREEN_HOME, HomeLayer.createArguments(homeId));
    }
}

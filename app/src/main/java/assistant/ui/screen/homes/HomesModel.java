package assistant.ui.screen.homes;

import android.support.annotation.NonNull;

import com.psliusar.layers.Model;

import java.util.List;

import assistant.model.home.Home;
import assistant.service.account.AccountService;
import assistant.service.data.DataService;
import io.reactivex.Observable;

public class HomesModel implements Model {

    private final DataService dataService;
    private final AccountService accountService;

    public HomesModel(@NonNull DataService dataService, @NonNull AccountService accountService) {
        this.dataService = dataService;
        this.accountService = accountService;
    }

    @NonNull
    Observable<Home> getHomeObservable() {
        return dataService.getHomeObservable();
    }

    @NonNull
    List<Home> getHomes() {
        return dataService.getAllHomes();
    }

    boolean isSignedIn() {
        return accountService.isSignedIn();
    }
}

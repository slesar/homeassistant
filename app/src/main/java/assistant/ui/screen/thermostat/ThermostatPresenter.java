package assistant.ui.screen.thermostat;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import java.util.List;

import assistant.model.thermostat.Thermostat;
import assistant.model.thermostat.ThermostatMode;
import assistant.service.api.ApiService;
import assistant.service.data.DataService;
import assistant.ui.screen.base.BasePresenter;
import assistant.ui.screen.thermostat.ThermostatModel.StatusItem;
import io.reactivex.android.schedulers.AndroidSchedulers;

public class ThermostatPresenter extends BasePresenter<ThermostatModel, ThermostatLayer> {

    private final DataService dataService;
    private final ApiService apiService;

    private Thermostat thermostat;

    public ThermostatPresenter(@NonNull DataService dataService, @NonNull ApiService apiService) {
        this.dataService = dataService;
        this.apiService = apiService;
    }

    @Override
    protected ThermostatModel onCreateModel() {
        return new ThermostatModel(dataService, apiService, getLayer().getDeviceId());
    }

    @Override
    public void start() {
        super.start();

        manage(getModel().getThermostatObservable()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::updateThermostat));

        updateThermostat(getModel().getThermostat());
    }

    @Override
    public void stop() {
        super.stop();
    }

    void setThermostatMode(@NonNull ThermostatMode mode) {
        getModel().setMode(mode);
    }

    void increaseUpperTemp() {
        getModel().setUpperTemp(thermostat.getUpperTemp() + 0.5D);
    }

    void decreaseUpperTemp() {
        getModel().setUpperTemp(thermostat.getUpperTemp() - 0.5D);
    }

    void increaseLowerTemp() {
        getModel().setLowerTemp(thermostat.getLowerTemp() + 0.5D);
    }

    void decreaseLowerTemp() {
        getModel().setLowerTemp(thermostat.getLowerTemp() - 0.5D);
    }

    void updateThermostatStatuses() {
        final List<StatusItem> items = getModel().getStatusItems();
        getLayer().statusAdapter.setItems(items);
    }

    void onStatusItemClick(@NonNull StatusItem item) {
        switch (item.type) {
            case FAN:

                break;
        }
    }

    private void updateThermostat(@Nullable Thermostat thermostat) {
        if (thermostat == null) {
            getLayer().getActivity().onBackPressed();
            return;
        }
        this.thermostat = thermostat;

        final ThermostatLayer layer = getLayer();
        layer.header.setTitle(thermostat.getName());

        final ThermostatMode mode = thermostat.getMode();
        final int upperTempsVisibility = mode == ThermostatMode.COOL || mode == ThermostatMode.RANGE ? View.VISIBLE : View.GONE;
        final int lowerTempsVisibility = mode == ThermostatMode.HEAT || mode == ThermostatMode.RANGE ? View.VISIBLE : View.GONE;
        switch (mode) {
            case ECONOMY:
                // TODO WTF?
                break;
            case OFF:
            default:
                break;
        }
        layer.iconCool.setVisibility(upperTempsVisibility);
        layer.coolTempUp.setVisibility(upperTempsVisibility);
        layer.coolTempDown.setVisibility(upperTempsVisibility);
        layer.targetCool.setVisibility(upperTempsVisibility);
        layer.iconHeat.setVisibility(lowerTempsVisibility);
        layer.heatTempUp.setVisibility(lowerTempsVisibility);
        layer.heatTempDown.setVisibility(lowerTempsVisibility);
        layer.targetHeat.setVisibility(lowerTempsVisibility);

        if (thermostat.canCool()) {
            layer.modeCool.setSelected(mode == ThermostatMode.COOL);
            layer.modeCool.setVisibility(View.VISIBLE);
        } else {
            layer.modeCool.setVisibility(View.GONE);
        }
        if (thermostat.canHeat()) {
            layer.modeHeat.setSelected(mode == ThermostatMode.HEAT);
            layer.modeHeat.setVisibility(View.VISIBLE);
        } else {
            layer.modeHeat.setVisibility(View.GONE);
        }
        if (thermostat.canRange()) {
            layer.modeRange.setSelected(mode == ThermostatMode.RANGE);
            layer.modeRange.setVisibility(View.VISIBLE);
        } else {
            layer.modeRange.setVisibility(View.GONE);
        }
        layer.modeOff.setSelected(mode == ThermostatMode.OFF);
        layer.currentTemperature.setText(thermostat.getCurrentTemp() + "");
        layer.targetCool.setText(thermostat.getUpperTemp() + "");
        layer.targetHeat.setText(thermostat.getLowerTemp() + "");
    }
}

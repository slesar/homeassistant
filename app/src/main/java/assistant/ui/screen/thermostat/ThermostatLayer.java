package assistant.ui.screen.thermostat;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.psliusar.layers.binder.Bind;
import com.psliusar.layers.binder.Binder;

import javax.inject.Inject;

import assistant.App;
import assistant.R;
import assistant.model.thermostat.ThermostatMode;
import assistant.service.api.ApiService;
import assistant.service.data.DataService;
import assistant.ui.adapter.BaseListAdapter;
import assistant.ui.adapter.BaseViewHolder;
import assistant.ui.adapter.BaseViewHolderFactory;
import assistant.ui.screen.base.BaseLayer;
import assistant.ui.screen.thermostat.ThermostatModel.StatusItem;
import assistant.ui.view.Header;
import assistant.ui.view.SwipeLayout;

public class ThermostatLayer extends BaseLayer<ThermostatPresenter> {

    private static final String ARGS_DEVICE_ID = "ARGS_DEVICE_ID";

    @NonNull
    public static Bundle createArguments(@NonNull String deviceId) {
        final Bundle bundle = new Bundle();
        bundle.putString(ARGS_DEVICE_ID, deviceId);
        return bundle;
    }

    @Inject
    ApiService apiService;

    @Inject
    DataService dataService;

    @Bind(R.id.header) Header header;

    @Bind(R.id.device_controls_swipe) SwipeLayout deviceControlsSwipe;
    @Bind(value = R.id.device_mode_cool, clicks = true) View modeCool;
    @Bind(value = R.id.device_mode_heat, clicks = true) View modeHeat;
    @Bind(value = R.id.device_mode_range, clicks = true) View modeRange;
    @Bind(value = R.id.device_mode_off, clicks = true) View modeOff;
    @Bind(R.id.device_current_temperature) TextView currentTemperature;
    @Bind(R.id.device_icon_cool) View iconCool;
    @Bind(R.id.device_icon_heat) View iconHeat;
    @Bind(R.id.device_target_cool_temp) TextView targetCool;
    @Bind(R.id.device_target_heat_temp) TextView targetHeat;
    @Bind(value = R.id.device_button_cool_temp_up, clicks = true) View coolTempUp;
    @Bind(value = R.id.device_button_cool_temp_down, clicks = true) View coolTempDown;
    @Bind(value = R.id.device_button_heat_temp_up, clicks = true) View heatTempUp;
    @Bind(value = R.id.device_button_heat_temp_down, clicks = true) View heatTempDown;

    @Bind(R.id.device_status_list)
    RecyclerView statusList;
    DeviceStatusAdapter statusAdapter;

    private String deviceId;

    public ThermostatLayer() {
        App.getGraph().inject(this);
    }

    @Override
    protected ThermostatPresenter onCreatePresenter() {
        return new ThermostatPresenter(dataService, apiService);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedState) {
        super.onCreate(savedState);
        deviceId = getArguments().getString(ARGS_DEVICE_ID);
    }

    @Nullable
    @Override
    protected View onCreateView(@Nullable ViewGroup viewGroup) {
        return inflate(R.layout.screen_thermostat, viewGroup);
    }

    @Override
    protected void onBindView(@NonNull View view) {
        super.onBindView(view);

        deviceControlsSwipe.setShadeDrawable(ContextCompat.getDrawable(getContext(), R.drawable.swipe_shade_left));

        statusAdapter = new DeviceStatusAdapter(getContext());
        statusAdapter.setOnClickListener(viewHolder -> {
            final StatusItem item = viewHolder.getData();
            if (item != null) {
                getPresenter().onStatusItemClick(item);
            }
        });

        statusList.setLayoutManager(new LinearLayoutManager(getContext()));
        statusList.setAdapter(statusAdapter);
        statusList.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));

        getPresenter().updateThermostatStatuses();
    }

    @Override
    protected void onDestroyView() {
        super.onDestroyView();
        statusAdapter = null;
    }

    public String getDeviceId() {
        return deviceId;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.device_mode_cool:
                getPresenter().setThermostatMode(ThermostatMode.COOL);
                break;
            case R.id.device_mode_heat:
                getPresenter().setThermostatMode(ThermostatMode.HEAT);
                break;
            case R.id.device_mode_range:
                getPresenter().setThermostatMode(ThermostatMode.RANGE);
                break;
            case R.id.device_mode_off:
                getPresenter().setThermostatMode(ThermostatMode.OFF);
                break;
            case R.id.device_button_cool_temp_up:
                getPresenter().increaseUpperTemp();
                break;
            case R.id.device_button_cool_temp_down:
                getPresenter().decreaseUpperTemp();
                break;
            case R.id.device_button_heat_temp_up:
                getPresenter().increaseLowerTemp();
                break;
            case R.id.device_button_heat_temp_down:
                getPresenter().decreaseLowerTemp();
                break;
            default:
                super.onClick(v);
        }
    }

    public static class DeviceStatusAdapter extends BaseListAdapter<StatusViewHolder, StatusItem> {

        public DeviceStatusAdapter(@NonNull Context context) {
            super(new StatusViewHolderFactory(context));
        }
    }

    public static class StatusViewHolderFactory extends BaseViewHolderFactory<StatusViewHolder, StatusItem> {

        public StatusViewHolderFactory(@NonNull Context context) {
            super(context);
        }

        @NonNull
        @Override
        public StatusViewHolder createViewHolder(@NonNull ViewGroup parent, int viewType) {
            final View view = inflate(R.layout.thermostat_status_list_item, parent);
            return new StatusViewHolder(view);
        }
    }

    public static class StatusViewHolder extends BaseViewHolder<StatusItem> {

        @Bind(R.id.icon) ImageView iconView;
        @Bind(R.id.title) TextView titleView;
        @Bind(R.id.subtitle) TextView subTitleView;

        public StatusViewHolder(@NonNull View itemView) {
            super(itemView);
            Binder.bind(this, itemView);
        }

        @Override
        public void bind(@Nullable StatusItem data) {
            super.bind(data);
            if (data == null) {
                data = new StatusItem();
            }
            iconView.setImageResource(data.iconRes);
            titleView.setText(data.title);
            subTitleView.setText(data.subTitle);
        }
    }
}

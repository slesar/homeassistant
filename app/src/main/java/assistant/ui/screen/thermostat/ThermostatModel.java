package assistant.ui.screen.thermostat;

import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.psliusar.layers.Model;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import assistant.model.thermostat.Thermostat;
import assistant.model.thermostat.ThermostatBackingResource;
import assistant.model.thermostat.ThermostatMode;
import assistant.service.api.ApiService;
import assistant.service.data.DataService;
import io.reactivex.Observable;

public class ThermostatModel implements Model {

    private final DataService dataService;
    private final String thermostatId;
    private final ThermostatBackingResource backingResource;

    public ThermostatModel(
            @NonNull DataService dataService,
            @NonNull ApiService apiService,
            @NonNull String thermostatId) {
        this.dataService = dataService;
        this.thermostatId = thermostatId;
        backingResource = apiService.getBackingResource(thermostatId);
    }

    @NonNull
    Observable<Thermostat> getThermostatObservable() {
        return dataService.getThermostatObservable()
                .filter(thermostat -> thermostat.getId().equals(thermostatId));
    }

    @Nullable
    Thermostat getThermostat() {
        return dataService.getThermostat(thermostatId);
    }

    void setUpperTemp(double temp) {
        final Thermostat thermostat = getThermostat();
        if (thermostat != null) {
            backingResource.setLowerTemp(thermostat, temp);
        }
    }

    void setLowerTemp(double temp) {
        final Thermostat thermostat = getThermostat();
        if (thermostat != null) {
            backingResource.setUpperTemp(thermostat, temp);
        }
    }

    void setMode(@NonNull ThermostatMode mode) {
        final Thermostat thermostat = getThermostat();
        if (thermostat != null) {
            backingResource.setMode(thermostat, mode);
        }
    }

    @NonNull
    List<StatusItem> getStatusItems() {
        final ArrayList<StatusItem> list = new ArrayList<>();
        final Thermostat thermostat = getThermostat();
        if (thermostat != null) {
            if (thermostat.hasHumidity()) {
                final StatusItem humidityItem = new StatusItem();
                humidityItem.type = StatusItemType.HUMIDITY;
                humidityItem.title = String.format(Locale.getDefault(), "Humidity: %d", thermostat.getHumidity());
                list.add(humidityItem);
            }

            if (thermostat.hasFan()) {
                final StatusItem fanItem = new StatusItem();
                fanItem.type = StatusItemType.FAN;
                fanItem.title = String.format(Locale.getDefault(), "Fan: %s", thermostat.getFanState().name());
                list.add(fanItem);
            }
        }
        return list;
    }

    public static class StatusItem {

        StatusItemType type;
        CharSequence title;
        CharSequence subTitle;
        @DrawableRes
        int iconRes;
    }

    public enum StatusItemType {
        HUMIDITY,
        FAN,
        SETUP
    }
}

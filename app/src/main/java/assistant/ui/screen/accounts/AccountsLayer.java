package assistant.ui.screen.accounts;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.psliusar.layers.binder.Bind;
import com.psliusar.layers.binder.Binder;

import javax.inject.Inject;

import assistant.App;
import assistant.R;
import assistant.ui.adapter.BaseListAdapter;
import assistant.ui.adapter.BaseViewHolder;
import assistant.ui.adapter.BaseViewHolderFactory;
import assistant.ui.screen.base.BaseLayer;
import assistant.ui.view.Header;
import nest.service.NestAuthService;

public class AccountsLayer extends BaseLayer<AccountsPresenter> {

    @Inject
    NestAuthService nestAuthService;

    @Bind(R.id.header) Header header;
    @Bind(R.id.accounts_list) RecyclerView accountsList;

    BaseListAdapter<AccountViewHolder, AccountsModel.Account> adapter;

    public AccountsLayer() {
        App.getGraph().inject(this);
    }

    @Override
    protected AccountsPresenter onCreatePresenter() {
        return new AccountsPresenter(nestAuthService);
    }

    @Nullable
    @Override
    protected View onCreateView(@Nullable ViewGroup parent) {
        return inflate(R.layout.screen_accounts, parent);
    }

    @Override
    protected void onBindView(@NonNull View view) {
        super.onBindView(view);

        final AccountActionListener actionListener = viewHolder -> {
            getPresenter().accountAction(viewHolder.getData().id);
        };

        final AccountViewHolderFactory factory = new AccountViewHolderFactory(getContext(), actionListener);
        adapter = new BaseListAdapter<>(factory);

        accountsList.setLayoutManager(new LinearLayoutManager(getContext()));
        accountsList.setAdapter(adapter);
        accountsList.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));

        getPresenter().updateAccountsList();
    }

    @Override
    protected void onDestroyView() {
        super.onDestroyView();
        adapter = null;
    }

    private static class AccountViewHolderFactory extends BaseViewHolderFactory<AccountViewHolder, AccountsModel.Account> {

        private final AccountActionListener listener;

        public AccountViewHolderFactory(@NonNull Context context, @NonNull AccountActionListener listener) {
            super(context);
            this.listener = listener;
        }

        @NonNull
        @Override
        public AccountViewHolder createViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new AccountViewHolder(inflate(R.layout.accounts_account_list_item, parent), listener);
        }
    }

    protected static class AccountViewHolder extends BaseViewHolder<AccountsModel.Account> {

        @Bind(R.id.icon) ImageView icon;
        @Bind(R.id.title) TextView title;
        @Bind(R.id.description) TextView description;
        @Bind(R.id.action) TextView actionButton;

        public AccountViewHolder(@NonNull View itemView, @NonNull AccountActionListener listener) {
            super(itemView);
            Binder.bind(this, itemView);

            actionButton.setOnClickListener(v -> listener.onActionCalled(AccountViewHolder.this));
        }

        @Override
        public void bind(@Nullable AccountsModel.Account account) {
            super.bind(account);
            if (account == null) {
                return;
            }
            icon.setImageResource(account.icon);
            title.setText(account.title);
            description.setText(account.description);
            actionButton.setText(account.actionTitle);
        }
    }

    public interface AccountActionListener {

        void onActionCalled(@NonNull AccountViewHolder viewHolder);
    }
}

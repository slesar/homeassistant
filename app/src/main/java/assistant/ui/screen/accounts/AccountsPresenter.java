package assistant.ui.screen.accounts;

import android.support.annotation.NonNull;

import assistant.ui.screen.ScreenRepo;
import assistant.ui.screen.accounts.AccountsModel.SignableService;
import assistant.ui.screen.auth.NestAuthLayer;
import assistant.ui.screen.base.BasePresenter;
import nest.service.NestAuthService;

public class AccountsPresenter extends BasePresenter<AccountsModel, AccountsLayer> {

    private final NestAuthService nestAuthService;

    private final NestAuthService.NestAuthListener nestAuthListener = new NestAuthService.NestAuthListener() {
        @Override
        public void onRequestAuthCode(@NonNull String requestUrl) {
            showScreen(ScreenRepo.SCREEN_AUTH_NEST, NestAuthLayer.requestUrlArguments(requestUrl));
        }

        @Override
        public void onAuthStateChanged(boolean authenticated) {

        }
    };

    public AccountsPresenter(@NonNull NestAuthService nestAuthService) {
        this.nestAuthService = nestAuthService;
    }

    @Override
    protected AccountsModel onCreateModel() {
        return new AccountsModel(nestAuthService);
    }

    @Override
    public void start() {
        super.start();
        nestAuthService.addListener(nestAuthListener);
        getLayer().header.setTitle("Accounts");
    }

    @Override
    public void stop() {
        super.stop();
        nestAuthService.removeListener(nestAuthListener);
    }

    void updateAccountsList() {
        if (getLayer().adapter != null) {
            getLayer().adapter.setItems(getModel().getAccounts());
        }
    }

    void accountAction(int accountId) {
        final SignableService service = getModel().getAccountServiceFromId(accountId);
        final int action = getModel().getActionFromId(accountId);

        switch (action) {
            case AccountsModel.ACTION_SIGN_IN:
                service.signIn();
                break;
            case AccountsModel.ACTION_SIGN_OUT:
                service.signOut();
                break;
        }
    }
}

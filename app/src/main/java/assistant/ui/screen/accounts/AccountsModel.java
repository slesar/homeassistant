package assistant.ui.screen.accounts;

import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;

import com.psliusar.layers.Model;

import java.util.ArrayList;

import assistant.R;
import nest.service.NestAuthService;

public class AccountsModel implements Model {

    public static final int ACCOUNT_NEST = 1;

    public static final int ACTION_SIGN_IN = 1 << 8;
    public static final int ACTION_SIGN_OUT = 2 << 8;

    private final NestAuthService nestAuthService;

    public AccountsModel(@NonNull NestAuthService nestAuthService) {
        this.nestAuthService = nestAuthService;
    }

    @NonNull
    ArrayList<Account> getAccounts() {
        final ArrayList<Account> accounts = new ArrayList<>();

        final Account account = new Account();
        final boolean nestAuth = nestAuthService.isAuthenticated();
        account.id = ACCOUNT_NEST | (nestAuth ? ACTION_SIGN_OUT : ACTION_SIGN_IN);
        account.icon = R.mipmap.ic_launcher;
        account.title = "Nest" + (nestAuth ? " (signed in)" : "");
        account.description = "Connect your Nest account";
        account.actionTitle = nestAuth ? "Sign out" : "Sign in";
        accounts.add(account);

        return accounts;
    }

    @NonNull
    SignableService getAccountServiceFromId(int id) {
        final int accountType = id & 0xff;
        switch (accountType) {
            case ACCOUNT_NEST:
                return new NestSignableService(nestAuthService);
            default:
                throw new IllegalArgumentException("Unknown account type: " + accountType);
        }
    }

    int getActionFromId(int id) {
        return id & (0xf << 8);
    }

    public static class Account {

        int id;
        CharSequence title;
        CharSequence description;
        CharSequence actionTitle;
        @DrawableRes
        int icon;
    }

    public interface SignableService {

        void signIn();

        void signOut();
    }

    public static class NestSignableService implements SignableService {

        private final NestAuthService nestAuthService;

        public NestSignableService(@NonNull NestAuthService nestAuthService) {
            this.nestAuthService = nestAuthService;
        }

        @Override
        public void signIn() {
            nestAuthService.logIn();
        }

        @Override
        public void signOut() {
            nestAuthService.logOut();
        }
    }
}

package assistant.ui.screen.home;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import assistant.model.home.Home;
import assistant.service.data.DataService;
import assistant.ui.screen.ScreenRepo;
import assistant.ui.screen.base.BasePresenter;
import assistant.ui.screen.thermostat.ThermostatLayer;
import io.reactivex.android.schedulers.AndroidSchedulers;

public class HomePresenter extends BasePresenter<HomeModel, HomeLayer> {

    private final DataService dataService;

    public HomePresenter(@NonNull DataService dataService) {
        this.dataService = dataService;
    }

    @Override
    protected HomeModel onCreateModel() {
        return new HomeModel(dataService, getLayer().getHomeId());
    }

    @Override
    public void start() {
        super.start();

        manage(getModel().getHomeObservable()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::updateHome));

        updateHome(getModel().getHome());
    }

    void updateDevicesList() {
        if (getLayer().adapter != null) {
            getLayer().adapter.setItems(getModel().getDevices());
        }

        //apiService.sendRequest(structure.getPath(structure.structureId, NestStructure.FIELD_AWAY), AwayState.AWAY.getKey());
        //apiService.sendRequest(structure.getPath(structure.structureId, NestStructure.FIELD_AWAY), AwayState.HOME.getKey());
    }

    void showThermostat(@NonNull String thermostatId) {
        showScreen(ScreenRepo.SCREEN_THERMOSTAT, ThermostatLayer.createArguments(thermostatId));
    }

    private void updateHome(@Nullable Home home) {
        if (home == null) {
            getLayer().getActivity().onBackPressed();
            return;
        }

        final HomeLayer layer = getLayer();
        layer.header.setTitle(home.getName());

        updateDevicesList();
    }
}

package assistant.ui.screen.home;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.psliusar.layers.Model;

import java.util.ArrayList;
import java.util.List;

import assistant.model.Device;
import assistant.model.home.Home;
import assistant.service.data.DataService;
import io.reactivex.Observable;

public class HomeModel implements Model {

    private final DataService dataService;
    private final String homeId;

    public HomeModel(@NonNull DataService dataService, @NonNull String homeId) {
        this.dataService = dataService;
        this.homeId = homeId;
    }

    @NonNull
    Observable<Home> getHomeObservable() {
        return dataService.getHomeObservable()
                .filter(home -> home.getId().equals(homeId));
    }

    @Nullable
    Home getHome() {
        return dataService.getHome(homeId);
    }

    @Nullable
    CharSequence getHomeTitle() {
        final Home home = dataService.getHome(homeId);
        return home == null ? null : home.getName();
    }

    @NonNull
    List<Device> getDevices() {
        final ArrayList<Device> devices = new ArrayList<>();
        final Home home = dataService.getHome(homeId);
        if (home == null) {
            return devices;
        }

        final List<String> deviceIds = home.getDeviceIds();
        for (String deviceId : deviceIds) {
            devices.add(dataService.getDevice(deviceId));
        }
        return devices;
    }
}

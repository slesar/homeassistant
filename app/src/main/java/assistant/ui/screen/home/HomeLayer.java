package assistant.ui.screen.home;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.psliusar.layers.binder.Bind;
import com.psliusar.layers.binder.Binder;

import java.util.List;

import javax.inject.Inject;

import assistant.App;
import assistant.R;
import assistant.model.Device;
import assistant.service.data.DataService;
import assistant.ui.adapter.BaseListAdapter;
import assistant.ui.adapter.BaseViewHolder;
import assistant.ui.adapter.BaseViewHolderFactory;
import assistant.ui.drawable.DebugDrawable;
import assistant.ui.screen.base.BaseLayer;
import assistant.ui.view.Header;

public class HomeLayer extends BaseLayer<HomePresenter> {

    private static final String ARGS_HOME_ID = "ARGS_HOME_ID";

    @Bind(R.id.header) Header header;
    @Bind(R.id.devices_list) RecyclerView homesList;

    @Inject
    DataService dataService;

    private String homeId;

    DevicesAdapter adapter;

    @NonNull
    public static Bundle createArguments(@NonNull String homeId) {
        final Bundle bundle = new Bundle();
        bundle.putString(ARGS_HOME_ID, homeId);
        return bundle;
    }

    public HomeLayer() {
        App.getGraph().inject(this);
    }

    @Override
    protected HomePresenter onCreatePresenter() {
        return new HomePresenter(dataService);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedState) {
        super.onCreate(savedState);
        homeId = getArguments().getString(ARGS_HOME_ID);
    }

    @Nullable
    @Override
    protected View onCreateView(@Nullable ViewGroup viewGroup) {
        return inflate(R.layout.screen_home, viewGroup);
    }

    @Override
    protected void onBindView(@NonNull View view) {
        super.onBindView(view);

        adapter = new DevicesAdapter(getContext());
        adapter.setOnClickListener(viewHolder -> getPresenter().showThermostat(viewHolder.getData().getId()));

        homesList.setLayoutManager(new LinearLayoutManager(getContext()));
        homesList.setAdapter(adapter);
        homesList.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));

        getPresenter().updateDevicesList();
    }

    @Override
    protected void onDestroyView() {
        super.onDestroyView();
        adapter = null;
    }

    @NonNull
    String getHomeId() {
        return homeId;
    }

    public static class DevicesAdapter extends BaseListAdapter<BaseViewHolder<Device>, Device> {

        public DevicesAdapter(@NonNull Context context) {
            super(new DeviceViewHolderFactory(context));
        }

        @Override
        public void setItems(@Nullable List<Device> items) {
            if (items != null) {
                items.add(0, null);
            }
            super.setItems(items);
        }

        @Override
        public int getItemViewType(int position) {
            if (position == 0) {
                return DeviceViewHolder.SPACER;
            }

            final Device device = getItem(position);
            if (device != null) {
                switch (device.getDeviceType()) {
                    case THERMOSTAT:
                        return DeviceViewHolder.DEVICE_THERMOSTAT;
                    case CAMERA:
                        return DeviceViewHolder.DEVICE_CAMERA;
                    case CO_ALARM:
                        break;
                }
            }

            throw new IllegalArgumentException("Unknown item type: " + device);
        }

        @Override
        public boolean isItemClickable(int position) {
            return position != 0;
        }
    }

    private static class DeviceViewHolderFactory extends BaseViewHolderFactory<BaseViewHolder<Device>, Device> {

        public DeviceViewHolderFactory(@NonNull Context context) {
            super(context);
        }

        @NonNull
        @Override
        public BaseViewHolder<Device> createViewHolder(@NonNull ViewGroup parent, int viewType) {
            switch (viewType) {
                case DeviceViewHolder.SPACER:
                    return new SpacerViewHolder(inflate(R.layout.home_device_list_item_spacer, parent));
                case DeviceViewHolder.DEVICE_THERMOSTAT:
                    return new ThermostatViewHolder(inflate(R.layout.home_device_list_item_thermostat, parent));
                case DeviceViewHolder.DEVICE_CAMERA:
                default:
                    throw new IllegalArgumentException("Unknown item type: " + viewType);

            }
        }
    }

    static abstract class DeviceViewHolder extends BaseViewHolder<Device> {

        static final int SPACER = 0;
        static final int DEVICE_THERMOSTAT = 1;
        static final int DEVICE_CAMERA = 2;

        private final int type;

        @Bind(R.id.title)
        TextView title;

        public DeviceViewHolder(@NonNull View itemView, int type) {
            super(itemView);
            this.type = type;
            Binder.bind(this, itemView);
        }

        @Override
        public void bind(@Nullable Device device) {
            super.bind(device);
            title.setText(device.getName());
        }
    }

    static class SpacerViewHolder extends BaseViewHolder<Device> {

        public SpacerViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }

    static class ThermostatViewHolder extends DeviceViewHolder {

        public ThermostatViewHolder(@NonNull View itemView) {
            super(itemView, DEVICE_THERMOSTAT);

            final DebugDrawable drawable = new DebugDrawable(null);
            drawable.setDefaultColor(0xffff8800);
            drawable.addGuide().setAngle(90);
            drawable.addGuide().setAngle(0);
            drawable.addGuide().setAngle(45);
            drawable.addCircle();
            drawable.addCircle()
                    .setScale(.5F)
                    .setGravity(DebugDrawable.GRAVITY_RIGHT | DebugDrawable.GRAVITY_BOTTOM);
            drawable.addRectangle();

            itemView.setBackgroundDrawable(drawable);
        }
    }

    static class ThermostatPresenter {

        public boolean canCool() {
            return false;
        }

        public boolean canHeat() {
            return false;
        }

        public boolean canRange() {
            return false;
        }

        public boolean hasFan() {
            return false;
        }

        public int getFanState() {
            return 0;
        }

        public float getUpperTemp() {
            return 0;
        }

        public float getLowerTemp() {
            return 0;
        }
    }
}

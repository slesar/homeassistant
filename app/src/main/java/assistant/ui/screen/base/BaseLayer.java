package assistant.ui.screen.base;

import android.support.annotation.NonNull;
import android.view.View;

import com.psliusar.layers.Layer;

import assistant.ui.activity.MainActivity;

public abstract class BaseLayer<P extends BasePresenter> extends Layer<P> implements CommonLayer {

    @Override
    protected void onBindView(@NonNull View view) {
        super.onBindView(view);
        getPresenter().start();
    }

    @Override
    protected void onDestroyView() {
        super.onDestroyView();
        getPresenter().stop();
    }

    @NonNull
    @Override
    public MainActivity getActivity() {
        return (MainActivity) super.getActivity();
    }
}

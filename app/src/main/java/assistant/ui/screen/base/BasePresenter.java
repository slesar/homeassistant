package assistant.ui.screen.base;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.psliusar.layers.Layer;
import com.psliusar.layers.Model;
import com.psliusar.layers.Presenter;

import assistant.ui.activity.MainActivity;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public abstract class BasePresenter<M extends Model, L extends Layer & CommonLayer> extends Presenter<M, L> {

    private final CompositeDisposable disposables = new CompositeDisposable();

    public void start() {

    }

    public void stop() {
        disposables.dispose();
    }

    protected void manage(@NonNull Disposable disposable) {
        disposables.add(disposable);
    }

    protected void showScreen(int screenId, @Nullable Bundle params) {
        ((MainActivity) getLayer().getActivity()).showScreen(screenId, params);
    }
}

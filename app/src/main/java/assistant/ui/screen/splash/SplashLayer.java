package assistant.ui.screen.splash;

import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;

import com.psliusar.layers.Layer;

import assistant.R;
import assistant.ui.screen.base.BaseLayer;

public class SplashLayer extends BaseLayer<SplashPresenter> {

    @Override
    protected SplashPresenter onCreatePresenter() {
        return new SplashPresenter();
    }

    @Nullable
    @Override
    protected View onCreateView(@Nullable ViewGroup viewGroup) {
        return inflate(R.layout.screen_splash, viewGroup);
    }
}

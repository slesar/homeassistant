package assistant.ui.screen;

import android.support.annotation.NonNull;

import com.psliusar.layers.Layer;
import com.psliusar.screens.Screen;

import assistant.ui.screen.accounts.AccountsLayer;
import assistant.ui.screen.auth.NestAuthLayer;
import assistant.ui.screen.home.HomeLayer;
import assistant.ui.screen.homes.HomesLayer;
import assistant.ui.screen.splash.SplashLayer;
import assistant.ui.screen.thermostat.ThermostatLayer;

public class ScreenRepo {

    /* ===== Screen IDs ===== */

    public static final int SCREEN_SPLASH = 1;

    public static final int SCREEN_ACCOUNTS = 101;

    public static final int SCREEN_SETUPS = 102;

    public static final int SCREEN_AUTH_NEST = 2;

    public static final int SCREEN_HOMES = 3;
    public static final int SCREEN_HOME = 4;

    public static final int SCREEN_THERMOSTAT = 5;
    public static final int SCREEN_CO_ALARM = 6;
    public static final int SCREEN_CAMERA = 7;

    /* ===== Factory ===== */

    public LayerScreen createScreen(int screenId) {
        switch (screenId) {
            case SCREEN_SPLASH:
                return new LayerScreen(SplashLayer.class, screenId).setName("Splash").setUpdateOnTop(true);

            case SCREEN_ACCOUNTS:
                return new LayerScreen(AccountsLayer.class, screenId).setName("Accounts").setUpdateOnTop(true);
            case SCREEN_AUTH_NEST:
                return new LayerScreen(NestAuthLayer.class, screenId).setName("NestAuth").setUpdateOnTop(true);

            case SCREEN_HOMES:
                return new LayerScreen(HomesLayer.class, screenId).setName("Homes").setClearStack(true).setUpdateOnTop(true);
            case SCREEN_HOME:
                return new LayerScreen(HomeLayer.class, screenId).setName("Home");

            case SCREEN_THERMOSTAT:
                return new LayerScreen(ThermostatLayer.class, screenId).setName("Thermostat");
            default:
                throw new IllegalArgumentException("Unknown screen ID:" + screenId);
        }
    }

    public static class LayerScreen extends Screen<LayerScreen> {

        public LayerScreen(@NonNull Class<? extends Layer> screenClass, int screenId) {
            super(screenClass, screenId);
        }
    }
}

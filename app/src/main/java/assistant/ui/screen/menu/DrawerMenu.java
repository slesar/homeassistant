package assistant.ui.screen.menu;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.View;

public class DrawerMenu implements DrawerLayout.DrawerListener {

    private DrawerLayout drawer;
    private MenuListener listener;
    private int mCurrentGravity = Gravity.LEFT;

    public void init(Activity activity, int containerId) {
        drawer = (DrawerLayout) activity.findViewById(containerId);
        drawer.addDrawerListener(this);
    }

    public boolean isOpen() {
        return drawer.isDrawerOpen(mCurrentGravity);
    }

    public void open() {
        drawer.openDrawer(mCurrentGravity);
    }

    public void close() {
        drawer.closeDrawer(mCurrentGravity);
    }

    public void setMenuListener(@Nullable MenuListener listener) {
        this.listener = listener;
    }

    public void setEnabled(boolean enabled) {
        drawer.setDrawerLockMode(enabled ? DrawerLayout.LOCK_MODE_UNLOCKED : DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    @Override
    public void onDrawerSlide(View drawerView, float slideOffset) {

    }

    @Override
    public void onDrawerOpened(View drawerView) {
        if (listener != null) {
            listener.onMenuOpened(this);
        }
    }

    @Override
    public void onDrawerClosed(View drawerView) {
        if (listener != null) {
            listener.onMenuClosed(this);
        }
    }

    @Override
    public void onDrawerStateChanged(int newState) {

    }

    public void setCurrentGravity(int gravity) {
        mCurrentGravity = gravity;
    }

    public interface MenuListener {

        void onMenuOpened(@NonNull DrawerMenu menu);

        void onMenuClosed(@NonNull DrawerMenu menu);
    }
}

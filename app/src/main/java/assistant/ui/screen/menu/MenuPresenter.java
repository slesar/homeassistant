package assistant.ui.screen.menu;

import com.psliusar.layers.Model;

import assistant.ui.screen.ScreenRepo;
import assistant.ui.screen.base.BasePresenter;

public class MenuPresenter extends BasePresenter<Model, MenuLayer> {

    public MenuPresenter() {

    }

    void showHome() {
        showScreen(ScreenRepo.SCREEN_HOMES, null);
    }

    void showAccount() {
        showScreen(ScreenRepo.SCREEN_ACCOUNTS, null);
    }

    void showSetups() {
        showScreen(ScreenRepo.SCREEN_SETUPS, null);
    }
}

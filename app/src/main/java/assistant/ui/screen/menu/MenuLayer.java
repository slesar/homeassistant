package assistant.ui.screen.menu;

import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;

import com.psliusar.layers.binder.Bind;

import assistant.R;
import assistant.ui.screen.base.BaseLayer;

public class MenuLayer extends BaseLayer<MenuPresenter> {

    @Bind(value = R.id.menu_home, clicks = true) View homeButton;
    @Bind(value = R.id.menu_account, clicks = true) View accountButton;
    @Bind(value = R.id.menu_setups, clicks = true) View setupsButton;

    @Override
    protected MenuPresenter onCreatePresenter() {
        return new MenuPresenter();
    }

    @Nullable
    @Override
    protected View onCreateView(@Nullable ViewGroup viewGroup) {
        return inflate(R.layout.screen_menu, viewGroup);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.menu_home:
                getPresenter().showHome();
                break;
            case R.id.menu_account:
                getPresenter().showAccount();
                break;
            case R.id.menu_setups:
                getPresenter().showSetups();
                break;
            default:
                super.onClick(v);
        }
    }
}

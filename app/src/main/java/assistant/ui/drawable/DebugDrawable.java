package assistant.ui.drawable;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.ArrayList;

public class DebugDrawable extends Drawable {

    public static final int GRAVITY_LEFT = 1;
    public static final int GRAVITY_TOP = 1 << 1;
    public static final int GRAVITY_RIGHT = 1 << 2;
    public static final int GRAVITY_BOTTOM = 1 << 3;
    public static final int GRAVITY_CENTER_VERTICAL = 1 << 4;
    public static final int GRAVITY_CENTER_HORIZONTAL = 1 << 5;
    public static final int GRAVITY_CENTER = GRAVITY_CENTER_VERTICAL | GRAVITY_CENTER_HORIZONTAL;

    private final Paint paint;
    private final Drawable background;
    private final ArrayList<Figure> figures = new ArrayList<>();
    private boolean dirty = true;

    private int defaultColor = 0xffff0000;
    private float defaultStrokeWidth = 1;

    public DebugDrawable(@Nullable Drawable background) {
        this.background = background;
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setStyle(Paint.Style.STROKE);
        paint.setPathEffect(new DashPathEffect(new float[] { 6F, 3F}, 0));
    }

    @Override
    protected void onBoundsChange(Rect bounds) {
        if (background != null) {
            background.setBounds(bounds);
        }

        reportDirty();
    }

    @Override
    public void draw(@NonNull Canvas canvas) {
        if (dirty) {
            rebuildPath();
        }

        if (background != null) {
            background.draw(canvas);
        }

        for (Figure figure : figures) {
            paint.setColor(defaultColor);
            paint.setStrokeWidth(defaultStrokeWidth);
            figure.draw(canvas, paint);
        }
    }

    @Override
    public void setAlpha(int alpha) {
        paint.setAlpha(alpha);
    }

    @Override
    public void setColorFilter(@Nullable ColorFilter colorFilter) {
        paint.setColorFilter(colorFilter);
    }

    @Override
    public int getOpacity() {
        return PixelFormat.TRANSLUCENT;
    }

    public void setDefaultColor(@ColorInt int defaultColor) {
        this.defaultColor = defaultColor;
        invalidateSelf();
    }

    public void setDefaultStrokeWidth(float defaultStrokeWidth) {
        this.defaultStrokeWidth = defaultStrokeWidth;
        invalidateSelf();
    }

    @NonNull
    public Guide addGuide() {
        final Guide guide = new Guide();
        figures.add(guide);
        reportDirty();
        return guide;
    }

    @NonNull
    public Circle addCircle() {
        final Circle circle = new Circle();
        figures.add(circle);
        reportDirty();
        return circle;
    }

    @NonNull
    public Rectangle addRectangle() {
        final Rectangle rectangle = new Rectangle();
        figures.add(rectangle);
        reportDirty();
        return rectangle;
    }

    private void reportDirty() {
        dirty = true;
        invalidateSelf();
    }

    private void rebuildPath() {
        final Rect bounds = getBounds();
        for (Figure figure : figures) {
            figure.updatePath(bounds);
        }
        dirty = false;
    }

    public static abstract class Figure<T extends Figure> {

        final Path path;

        @Nullable
        Float strokeWidth;
        @Nullable
        Integer color;
        @Nullable
        Point offset;
        int gravity;
        float scale = 1F;

        public Figure() {
            path = new Path();
        }

        @NonNull
        public T setColor(@Nullable Integer color) {
            this.color = color;
            return (T) this;
        }

        @NonNull
        public T setStrokeWidth(@Nullable Float strokeWidth) {
            this.strokeWidth = strokeWidth;
            return (T) this;
        }

        @NonNull
        public T setOffset(@Nullable Point offset) {
            this.offset = offset;
            return (T) this;
        }

        @NonNull
        public T setGravity(int gravity) {
            this.gravity = gravity;
            return (T) this;
        }

        @NonNull
        public T setScale(float scale) {
            this.scale = scale;
            return (T) this;
        }

        protected void draw(@NonNull Canvas canvas, @NonNull Paint paint) {
            if (color != null) {
                paint.setColor(color);
            }
            if (strokeWidth != null) {
                paint.setStrokeWidth(strokeWidth);
            }
            canvas.drawPath(path, paint);
        }

        protected boolean hasGravity(int gravityMode) {
            return (gravity & gravityMode) == gravityMode;
        }

        protected abstract void updatePath(@NonNull Rect bounds);
    }

    public static class Guide extends Figure<Guide> {

        float angle;

        @NonNull
        public Guide setAngle(float angle) {
            this.angle = angle;
            return this;
        }

        @Override
        protected void updatePath(@NonNull Rect bounds) {
            path.reset();

            final int width = bounds.width();
            final int height = bounds.height();

            double centerX;
            double centerY;

            if (hasGravity(GRAVITY_LEFT)) {
                centerX = width * (1 - scale);
            } else if (hasGravity(GRAVITY_RIGHT)) {
                centerX = (width - 1) * scale;
            } else if (hasGravity(GRAVITY_CENTER_HORIZONTAL)) {
                centerX = (width / 2D) * scale;
            } else {
                centerX = (width / 2D) * scale;
            }

            if (hasGravity(GRAVITY_TOP)) {
                centerY = height * (1 - scale);
            } else if (hasGravity(GRAVITY_BOTTOM)) {
                centerY = (height - 1) * scale;
            } else if (hasGravity(GRAVITY_CENTER_VERTICAL)) {
                centerY = (height / 2D) * scale;
            } else {
                centerY = (height / 2D) * scale;
            }

            if (offset != null) {
                centerX += offset.x;
                centerY += offset.y;
            }

            final double radians = Math.toRadians(angle);
            final double length = Math.max(width, height);
            final float startX = (float) (centerX + Math.cos(radians) * length);
            final float startY = (float) (centerY + Math.sin(radians) * length);
            final float finishX = (float) (centerX - Math.cos(radians) * length);
            final float finishY = (float) (centerY - Math.sin(radians) * length);

            path.moveTo(startX, startY);
            path.quadTo(startX, startY, finishX, finishY);
        }
    }

    public static class Rectangle extends Figure {

        @Override
        protected void updatePath(@NonNull Rect bounds) {
            path.reset();

            final RectF rect = new RectF(bounds);
            rect.right--;
            rect.bottom--;
            if (offset != null) {
                rect.offset(offset.x, offset.y);
            }
            rect.inset(rect.width() * (1 - scale) / 2F, rect.height() * (1 - scale) / 2F);

            if (hasGravity(GRAVITY_LEFT)) {
                rect.offsetTo(0, rect.top);
            } else if (hasGravity(GRAVITY_RIGHT)) {
                rect.offsetTo(bounds.width() - 1 - rect.width(), rect.top);
            } else if (hasGravity(GRAVITY_CENTER_HORIZONTAL)) {
                // ignore, already centered
            }

            if (hasGravity(GRAVITY_TOP)) {
                rect.offsetTo(rect.left, 0);
            } else if (hasGravity(GRAVITY_BOTTOM)) {
                rect.offsetTo(rect.left, bounds.height() - 1 - rect.height());
            } else if (hasGravity(GRAVITY_CENTER_VERTICAL)) {
                // ignore, already centered
            }

            path.addRect(rect, Path.Direction.CW);
        }
    }

    public static class Circle extends Figure {

        @Override
        protected void updatePath(@NonNull Rect bounds) {
            path.reset();

            final int width = bounds.width();
            final int height = bounds.height();

            float centerX;
            float centerY;

            final float radius = Math.min(width / 2F, height / 2F) * scale;

            if (hasGravity(GRAVITY_LEFT)) {
                centerX = radius;
            } else if (hasGravity(GRAVITY_RIGHT)) {
                centerX = width - radius;
            } else if (hasGravity(GRAVITY_CENTER_HORIZONTAL)) {
                centerX = width / 2F;
            } else {
                centerX = width / 2F;
            }

            if (hasGravity(GRAVITY_TOP)) {
                centerY = radius;
            } else if (hasGravity(GRAVITY_BOTTOM)) {
                centerY = height - radius;
            } else if (hasGravity(GRAVITY_CENTER_VERTICAL)) {
                centerY = height / 2F;
            } else {
                centerY = height / 2F;
            }

            if (offset != null) {
                centerX += offset.x;
                centerY += offset.y;
            }

            path.addCircle(centerX, centerY, radius, Path.Direction.CW);
        }
    }
}

package assistant.model.thermostat;

public enum ThermostatMode {
    COOL,
    HEAT,
    RANGE,
    ECONOMY,
    OFF
}

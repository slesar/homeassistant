package assistant.model.thermostat;

import android.support.annotation.NonNull;

import assistant.model.Device;

public class Thermostat extends Device {

    private ThermostatMode mode;

    private boolean canCool;
    private boolean canHeat;
    private boolean canRange;

    private boolean hasFan;
    private FanState fanState;

    private double upperTemp;
    private double lowerTemp;
    private double currentTemp;

    private boolean hasHumidity;
    private int humidity;

    private boolean online;

    public Thermostat(@NonNull String id) {
        super(DeviceType.THERMOSTAT, id);
    }

    public boolean canCool() {
        return canCool;
    }

    public void setCanCool(boolean value) {
        canCool = value;
    }

    public boolean canHeat() {
        return canHeat;
    }

    public void setCanHeat(boolean value) {
        canHeat = value;
    }

    public boolean canRange() {
        return canRange;
    }

    public void setCanRange(boolean value) {
        canRange = value;
    }

    public boolean hasFan() {
        return hasFan;
    }

    public void setHasFan(boolean value) {
        hasFan = value;
    }

    @NonNull
    public FanState getFanState() {
        return fanState == null ? FanState.NONE : fanState;
    }

    public void setFanState(@NonNull FanState state) {
        fanState = state;
    }

    public double getUpperTemp() {
        return upperTemp;
    }

    public void setUpperTemp(double value) {
        upperTemp = value;
    }

    public double getLowerTemp() {
        return lowerTemp;
    }

    public void setLowerTemp(double value) {
        lowerTemp = value;
    }

    public double getCurrentTemp() {
        return currentTemp;
    }

    public void setCurrentTemp(double value) {
        currentTemp = value;
    }

    public boolean hasHumidity() {
        return hasHumidity;
    }

    public void setHasHumidity(boolean value) {
        hasHumidity = value;
    }

    public int getHumidity() {
        return humidity;
    }

    public void setHumidity(int value) {
        humidity = value;
    }

    public boolean isOnline() {
        return online;
    }

    public void setOnline(boolean value) {
        online = value;
    }

    @NonNull
    public ThermostatMode getMode() {
        return mode;
    }

    public void setMode(@NonNull ThermostatMode mode) {
        this.mode = mode;
    }
}

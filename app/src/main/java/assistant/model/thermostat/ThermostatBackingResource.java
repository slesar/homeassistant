package assistant.model.thermostat;

import android.support.annotation.NonNull;

public interface ThermostatBackingResource {

    double setUpperTemp(@NonNull Thermostat thermostat, double value);

    double setLowerTemp(@NonNull Thermostat thermostat, double value);

    @NonNull
    ThermostatMode setMode(@NonNull Thermostat thermostat, @NonNull ThermostatMode mode);
}

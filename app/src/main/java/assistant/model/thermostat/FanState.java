package assistant.model.thermostat;

public enum FanState {
    NONE,
    ON,
    OFF
}

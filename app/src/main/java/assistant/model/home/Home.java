package assistant.model.home;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import assistant.model.IdentifiedEntity;

public class Home extends IdentifiedEntity {

    private List<String> deviceIds = new ArrayList<>();
    private HomeAwayState homeAwayState;

    public Home(@NonNull String id) {
        super(id);
    }

    public void addDeviceId(@NonNull String deviceId) {
        deviceIds.add(deviceId);
    }

    @NonNull
    public List<String> getDeviceIds() {
        final ArrayList<String> ids = new ArrayList<>();
        ids.addAll(deviceIds);
        return ids;
    }

    @NonNull
    public HomeAwayState getHomeAwayState() {
        return homeAwayState == null ? HomeAwayState.AWAY : homeAwayState;
    }

    public void setHomeAwayState(@NonNull HomeAwayState state) {
        homeAwayState = state;
    }
}

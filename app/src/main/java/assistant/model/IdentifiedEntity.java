package assistant.model;

import android.support.annotation.NonNull;

public abstract class IdentifiedEntity {

    private final String id;
    private String name;

    public IdentifiedEntity(@NonNull String id) {
        this.id = id;
    }

    @NonNull
    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

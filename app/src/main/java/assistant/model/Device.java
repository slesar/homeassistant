package assistant.model;

import android.support.annotation.NonNull;

public abstract class Device extends IdentifiedEntity {

    public enum DeviceType {
        THERMOSTAT,
        CAMERA,
        CO_ALARM
    }

    private final DeviceType deviceType;

    public Device(@NonNull DeviceType type, @NonNull String id) {
        super(id);
        deviceType = type;
    }

    @NonNull
    public DeviceType getDeviceType() {
        return deviceType;
    }
}

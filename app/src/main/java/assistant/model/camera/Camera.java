package assistant.model.camera;

import android.support.annotation.NonNull;

import assistant.model.Device;

public class Camera extends Device {

    public Camera(@NonNull String id) {
        super(DeviceType.CAMERA, id);
    }
}

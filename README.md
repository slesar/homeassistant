# README #

An unfinished project showing how to work with "Works with Nest" API. Made an attempt to completely separate API's logic from the main app's logic.

### Dependencies ###

* Uses Layers library which is not published yet. Can be checked out from GitHub (https://github.com/slesar/Layers) and published to the local Maven repository.
